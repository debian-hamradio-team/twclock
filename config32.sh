#! /bin/sh

###   
### Use this script to configure for a 32 bit compile on a 64 bit PC
###   

./configure --build=i386-pc-linux-gnu "CFLAGS=-m32" "LDFLAGS=-m32"
