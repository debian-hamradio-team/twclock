twclock (3.5-2) unstable; urgency=medium

  * Team upload.

  [ Christoph Berg ]
  * Add Vcs fields.
  * Retire Kamal and Colin from Uploaders. (Closes: #990590)

  [ Daniele Forsi ]
  * Use versioned copyright format URI.
  * Set debhelper-compat version in Build-Depends.
  * Fix day-of-week for changelog entry 1.1-1.
  * Update standards version to 4.6.0, no changes needed.
  * Drop d/p/23-remove-exec-from-desktop.patch and menu file.
  * Add Comment[it] to .desktop file.

  [ Ervin Hegedus ]
  * Add d/p/25-fixftbfs-gcc15.patch (Closes: #1098021)

 -- Christoph Berg <myon@debian.org>  Tue, 18 Feb 2025 16:15:41 +0100

twclock (3.5-1) unstable; urgency=medium

  * New upstream release
  * Fixed gcc 10 errors in upstream (Closes: #957890)
  * Bumb Standard Version
    + added Rules-Requires-Root field to d/changelog
    + Aligned 01-change-button-text-to-Alarm-on.patch for new version
    + Removed trailing whitespaces from d/*
    + Added patch to remove the Exec tag from desktop file
  * Bump compat version to 13

 -- Ervin Hegedus <airween@gmail.com>  Sun, 17 May 2020 14:30:43 +0000

twclock (3.4-2) unstable; urgency=medium

  * Changed architecture from "any" to "linux-any"
    (there are some Linux-specific dependencies)

 -- Ervin Hegedus <airween@gmail.com>  Thu, 07 Sep 2017 15:52:03 +0000

twclock (3.4-1) unstable; urgency=medium

  * New upstream release
  * Bumb Standard Version (No changes required)
  * Bump compat level to 10
    + Removed dh_autoreconf_override from rules (compat > 10)
    + Removed autoconf-dev at Build-Depends in control
    + Added hardening switches to rule file, instead of dpkg-buildflags
  * Aligned d/patches/20-rename-upstream-ChangeLog.patch to new content

 -- Ervin Hegedus <airween@gmail.com>  Thu, 07 Sep 2017 12:01:30 +0000

twclock (3.3-2) unstable; urgency=medium

  * Fix homepage in debian/control - http not https.

 -- Colin Tuckley <colint@debian.org>  Mon, 16 Jun 2014 17:30:23 +0100

twclock (3.3-1) unstable; urgency=medium

  * New upstream release (Closes: #727994, #744532)
  * Update Homepage URL since site has moved (Closes: #739475)
  * Bump Standards version (No changes required)
  * Ack the NMU
  * Update watch file for new URL

 -- Colin Tuckley <colint@debian.org>  Tue, 10 Jun 2014 11:06:29 +0100

twclock (3.1-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Transition from lesstif2 to motif (Closes: 714669).

 -- Luk Claes <luk@debian.org>  Sat, 14 Sep 2013 18:06:38 +0200

twclock (3.1-2) unstable; urgency=high

  * Fix underlinking: link libpulse explicitly
    - Thanks Graham Inggs (Closes: #701900, LP: #1134240)
  * Enabled build-hardening

 -- Kamal Mostafa <kamal@whence.com>  Thu, 28 Feb 2013 11:42:51 -0800

twclock (3.1-1) unstable; urgency=low

  * New upstream release
    - features PulseAudio support
    - all new debian/ packaging
  * New Debian maintainer (Closes: #648258).

 -- Kamal Mostafa <kamal@whence.com>  Sat, 12 Nov 2011 10:46:14 -0800

twclock (2.7-3) unstable; urgency=low

  * Retiring - set the package maintainer to Debian QA Group.

 -- Joop Stakenborg <pa3aba@debian.org>  Tue, 03 Nov 2009 07:09:53 +0000

twclock (2.7-2) unstable; urgency=low

  * Change the alarm timer button label. It now reads "Alarm On" instead
    of the default "TWCLOCK". Closes: #276094.

 -- Joop Stakenborg <pa3aba@debian.org>  Wed, 12 Dec 2007 18:58:53 +0100

twclock (2.7-1) unstable; urgency=low

  * New upstream release.
  * Menu transition.
  * Lintian clean.

 -- Joop Stakenborg <pa3aba@debian.org>  Thu, 18 Oct 2007 19:52:36 +0200

twclock (2.5-4) unstable; urgency=low

  * Add xutils-dev to the build dependencies. We need xmkmf, which is used
    by the m4 scripts to determine the application defaults directory.
    Closes: #374346.

 -- Joop Stakenborg <pa3aba@debian.org>  Fri, 23 Jun 2006 21:17:52 +0200

twclock (2.5-3) unstable; urgency=low

  * Build-depends lesstif2-dev. Closes: #374234.

 -- Joop Stakenborg <pa3aba@debian.org>  Sun, 18 Jun 2006 19:45:10 +0200

twclock (2.5-2) unstable; urgency=low

  * Fix building on archs which still use /usr/X11R6/lib/X11/app-defaults.
    Closes: #349434.

 -- Joop Stakenborg <pa3aba@debian.org>  Mon, 23 Jan 2006 18:31:26 +0100

twclock (2.5-1) unstable; urgency=low

  * New upstream release.
  * Acknowledge NMU, thanks Marc. Closes: #346901.
  * Migrate to debhelper.

 -- Joop Stakenborg <pa3aba@debian.org>  Sun, 22 Jan 2006 18:02:00 +0100

twclock (2.3-1.1) unstable; urgency=low

  * Non-maintainer upload to help with xlibs-dev transition
  * debian/control: Replaced xlibs-dev build-dep. (Closes: #346901)

 -- Marc 'HE' Brockschmidt <he@debian.org>  Fri, 20 Jan 2006 17:03:11 +0100

twclock (2.3-1) unstable; urgency=low

  * New upstream. I seem to have missed a minor version somehow.
  * Add CHANGES file to the package.

 -- Joop Stakenborg <pa3aba@debian.org>  Sat, 31 Jul 2004 21:08:14 +0200

twclock (2.1-2) unstable; urgency=low

  * Use /usr/share/doc/twclock/README.* in the manual page. Closes: #260907.
  * Also change README.debian a bit. Thanks Dan.
  * Move the binary to /usr/bin, manual page to /usr/share/man/man1, pixmaps
    to /usr/share/pixmaps according to debian policy.

 -- Joop Stakenborg <pa3aba@debian.org>  Sat, 31 Jul 2004 19:57:06 +0200

twclock (2.1-1) unstable; urgency=low

  * New upstream.

 -- Joop Stakenborg <pa3aba@debian.org>  Sat,  8 Mar 2003 20:37:46 +0100

twclock (2.0-1) unstable; urgency=low

  * New upstream.

 -- Joop Stakenborg <pa3aba@debian.org>  Sun, 16 Feb 2003 17:42:57 +0100

twclock (1.4-1) unstable; urgency=low

  * New upstream release.

 -- Joop Stakenborg <pa3aba@debian.org>  Wed, 21 Nov 2001 18:39:30 +0100

twclock (1.3-16) unstable; urgency=low

  * Remove tzwatch from the package now that Drew has packages it.
  * Suggest tzwatch and gworldclock.
  * Fix 2 lintian warnings (never seen before).

 -- Joop Stakenborg <pa3aba@debian.org>  Sat,  1 Sep 2001 21:07:25 +0200

twclock (1.3-15) unstable; urgency=low

  * Remove libc6-dev from the Build-Depends line as per debian policy.
  * Make the app-defaults file Twclock a conffile.

 -- Joop Stakenborg <pa3aba@debian.org>  Tue, 10 Jul 2001 21:47:33 +0200

twclock (1.3-14) unstable; urgency=low

  * Remove dot from the build-depends line (thanks paul).

 -- Joop Stakenborg <pa3aba@debian.org>  Wed, 14 Feb 2001 19:31:14 +0100

twclock (1.3-13) unstable; urgency=low

  * Add debmake to the build-depends. Closes: #85417.

 -- Joop Stakenborg <pa3aba@debian.org>  Sat, 10 Feb 2001 09:42:57 +0100

twclock (1.3-12) unstable; urgency=low

  * Update standards version.
  * Move app-defaults file to right directory.
  * Add build-depends (closes #84875)
  * Fix a one year old bug which caused twclock to always jump to $HOME
    the second time a timezone is selected. I did this by providing an
    argument to XmCreateFileSelectionDialog. The path to /usr/share/zoneinfo
    is now hardcoded into this argument (closes #51658)
  * Comment out twclock*zone_fsb.directory in Twclock app-defaults file.

 -- Joop Stakenborg <pa3aba@debian.org>  Thu,  8 Feb 2001 11:00:26 +0100

twclock (1.3-11) unstable; urgency=low

  * Oops. Should not upload to frozen.

 -- Joop Stakenborg <pa3aba@debian.org>  Sat, 12 Aug 2000 11:43:49 +0200

twclock (1.3-10) frozen unstable; urgency=low

  * Update tzwatch.
  * Recompile against new lesstif version.
  * Let dpkg-shlibdeps figure out dependencies, closes bug #67693.
  * Fixed lintian warnings.

 -- Joop Stakenborg <pa3aba@debian.org>  Tue, 25 Jul 2000 21:32:08 +0200

twclock (1.3-9) frozen unstable; urgency=low

  * Surprise! Tzselect is back in the archive again. Have to revert the
    changes.
  * Make twclock depend a libc6 version which provides tzselect.

 -- Joop Stakenborg <pa3aba@debian.org>  Fri, 28 Jan 2000 11:24:23 +0100

twclock (1.3-8) frozen unstable; urgency=low

  * Tzselect is removed from the archive. I have clarified this in
    README.tzselect. This file also contains a copy of tzselect, so users
    can install it locally. This way tzwatch can still be used.
    closes bug #56269.

 -- Joop Stakenborg <pa3aba@debian.org>  Wed, 26 Jan 2000 21:04:43 +0100

twclock (1.3-7) unstable; urgency=low

  * Small change to tzwatch manual page.

 -- Joop Stakenborg <pa3aba@debian.org>  Fri, 31 Dec 1999 09:58:05 +0100

twclock (1.3-6) unstable; urgency=low

  * Fixed localtime selection.
  * Include Drew Parsons' tzwatch.
  * Wrote manual page for tzwatch.

 -- Joop Stakenborg <pa3aba@debian.org>  Wed,  1 Dec 1999 19:04:48 +0100

twclock (1.3-5) unstable; urgency=low

  * Newpath now is a global variable. It would return 'null' when a timezone
    was selected, after destruction of the widget (closes: Bug#51352).
  * Lots of fprintf lines added to the code, in case I have to debug this again.
    This can be enabled with the -DBUG compiler directive.
    See makefile.debian.
  * Fixed call to XtDestroyWidget. Could be a lesstif bug.

 -- Joop Stakenborg <pa3aba@debian.org>  Sun, 28 Nov 1999 17:56:18 +0100

twclock (1.3-4) unstable; urgency=low

  * Recompile against new lesstif1 package.

 -- Joop Stakenborg <pa3aba@debian.org>  Sun, 17 Oct 1999 09:43:15 +0200

twclock (1.3-3) unstable; urgency=low

  * Typo.

 -- Joop Stakenborg <pa3aba@debian.org>  Thu, 14 Oct 1999 18:43:15 +0200

twclock (1.3-2) unstable; urgency=low

  * Updated standards version.
  * FHS compliant.

 -- Joop Stakenborg <pa3aba@debian.org>  Tue,  5 Oct 1999 18:03:02 +0200

twclock (1.3-1) unstable; urgency=low

  * New upstream release.

 -- Joop Stakenborg <pa3aba@debian.org>  Fri,  5 Mar 1999 12:14:32 +0100

twclock (1.2-1) unstable; urgency=low

  * New upstream version.
  * Architecture is 'any' now (james@nocrew.org). Fixes bug #28499.

 -- Joop Stakenborg <pa3aba@debian.org>  Tue, 27 Oct 1998 12:17:59 +0100

twclock (1.1-3) unstable; urgency=low

  * Manpage now put in the right directory.

 -- Joop Stakenborg <pa3aba@debian.org>  Tue, 13 Oct 1998 12:27:31 +0200

twclock (1.1-2) unstable; urgency=low

  * Menu entry moved to Hamradio.
  * Mini-icon for the menu now put in the right place.
    Fixes bug #26481.

 -- Joop Stakenborg <pa3aba@debian.org>  Mon,  5 Oct 1998 21:07:59 +0200

twclock (1.1-1) unstable; urgency=low

  * Initial Release.
  * Manual page added.

 -- Joop Stakenborg <pa3aba@debian.org>  Wed, 10 Jun 1998 21:02:16 +0100
