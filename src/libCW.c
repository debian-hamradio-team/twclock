/* 
 * libCW - Copyright (C) 2006-2018 by Ted Williams - WA0EIR
 * Derived mostly from qrq.c - High speed morse trainer
 * by Fabian Kurz, DJ1YFK, Copyright (C) 2006, and
 * a few bits from unixcw
 * by Simon Baldwin, G0FRD, Copyright (C) 2001-2006
 * 
 * This file is part of libCW
 *
 * libCW is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>
 */ 

/*
 * libCW.c
 */
#include "libCW.h"

#include <errno.h>
#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <fcntl.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/kd.h>
#include <sys/msg.h>
#include <sys/ioctl.h>
#include <linux/rtc.h>

#include <pulse/simple.h>
#include <pulse/error.h>

/*
 * set these as needed by the application
 */
#define TESTMODE  0
#define MESG      0               /* 0=no message, 1=with message support */
#define DEBUG     0

#define MAX_USER_FREQ 3072
#define MAX_NAME  20
#define MAX_LETTERS 512
#define MAX_WORD_LENGTH  100L     /* Maximum length of a word */
#define PI 3.1415926              /* Indiana residents, feel free to */
                                  /* change it to 3.2 */
#define DIT '.'
#define DAH '-'

//#define SAMPLERATE 8000           /* take your pick */
#define SAMPLERATE 44100
//#define SAMPLERATE 48000

#define TRUE 1
#define FALSE !TRUE
#define SENT_MSG 9L               /* as defined in common.h */
#define SENT_WPM 8L               /* as defined in common.h */

#if MESG == 1
extern int qid;
#endif

/* set some variables to their default values */
int qid = 0;
char serdevice[MAX_NAME] = "";    /* default serial device path */
char pa_name[MAX_NAME] = "";      /* shows up on pa vol ctrl */
int ser_fd = 0;
int keyMode = PA;                 /* PA or RTS */

int rts_on = TIOCM_RTS;           /* for key down */
int rts_off = 0;                  /* for key up */
int initialized = 0;              /* set to 1 by cw_init */

/* for PA */
static pa_simple *sp = NULL;      /* pulse audio pointer */

static const pa_sample_spec ss =
{
   .format = PA_SAMPLE_S16LE,
   .rate = SAMPLERATE,
   .channels = 2
};

static const pa_buffer_attr ba =
{
   .maxlength = -1,
   .prebuf = SAMPLERATE/128
};


char *codetable[MAX_LETTERS];

/* set some default values */
int   msg_mode =  0;                       /* 0 = no 1 = yes */
int   rtc_rate = 64;                       /* RTC interupt rate */
float wpm = DEFAULT_WPM;                   /* default speed*/
float dit_time = 1200.0 / DEFAULT_WPM;
float dah_time = 1200.0 / DEFAULT_WPM * 3; /* dah time in (ms) */

float farns_wpm = DEFAULT_FARNS_OFF;       /* farnsworth wpm - default = 7 */
int   farns_off = DEFAULT_FARNS_OFF;       /* farns is off wpm */
float farns_time;                          /* farns time */

float ic_time = 1200.0 / DEFAULT_WPM;      /* intra letter time ms */
float il_time = 3 * 1200.0 / DEFAULT_WPM;  /* inter letter time ms */
float iw_time = 7 * 1200.0 / DEFAULT_WPM;  /* inter word time ms */

float rise = DEFAULT_RT;                   /* risetime in ms */
float fall = DEFAULT_FT;                   /* fall time in ms */
float weight = DEFAULT_WT;                 /* weight time */
float ratio = DEFAULT_RATIO;               /* ratio */
float dit_factor = 1.0, dah_factor = 1.0;

float rt = DEFAULT_RT/1000.0*SAMPLERATE;   /* rise time in samples */
float ft = DEFAULT_RT/1000.0*SAMPLERATE;   /* fall time in samples */
float wt = DEFAULT_WT/1000.0*SAMPLERATE;   /* weight in samples */

/* tau for RC time constants  - 4 time constants per r/f time */
float tau_r = (DEFAULT_RT/1000.0)/4.0;     /* rise time constant */
float tau_f = (DEFAULT_FT/1000.0)/4.0;     /* fall time constant */

int   shape = DEFAULT_SHAPE;               /* default shape */

int   tone = DEFAULT_TONE;                 /* default tone for SC */
float omega = 2.0*PI*DEFAULT_TONE;         /* omega = 2*PI*f */
float a0, a1, a2, a3;

/* forward declarations for internal functions */
int  calc_wpm (int new_wpm);
void *send_cw (void *p);
void keySC (char *code);
void keyRTS (char *code);
int  open_serial (void);
int  timer (float usec);
int  close_devs(void);
int  tonegen (int tone, float length);


/*
 * cw_init function  
 * check to see if PA or RTS, and copy newmode and ser to local variable.
 * PA needs to create a new pa stream.  RTS needs to open the serial card.
 * Build the character table and setup traps.
 */
int cw_init (int newmode, char name[], char ser[])
{
   int i, rtn, error;

   /* always save the serial device and PA name */
   if (strlen (ser) == 0)
   {
     fprintf (stderr, "Error cw_init: serial device argument is NULL\n");
     return -2;
   }
   else
   {
      strncpy (serdevice, ser, MAX_NAME - 1);  /* copy ser to serdevice */
   }

   /* always save the PA name */
   /* check for a name argument for pa volume control */
   if (strlen (name) == 0)
   {
      fprintf (stderr, "Error cw_init: name argument is NULL\n");
      return -1;
   }
   else
   {
      strncpy (pa_name, name, MAX_NAME - 1);  /* copy name to pa_name */
      /* check pulseaudio pointer and open if it isn't */
      if (sp == NULL)
      {
          sp = pa_simple_new (NULL, pa_name, PA_STREAM_PLAYBACK, NULL,
                              pa_name, &ss, NULL, &ba, &error);
      }
   }
   
   /* now set the requested mode */
   switch (newmode)
   {
      case PA:
         if (cw_set_keyMode (PA, pa_name) != PA)       /* set keyMode to PA */
         {
            fprintf (stderr, "Error cw_init: set to PA failed\n");
            return -3;
         }

         #if DEBUG == 1
         fprintf (stderr, "cw_init - to PA mode\n");
         #endif

         break;

      case RTS:
         if ((cw_set_keyMode (RTS, serdevice)) != RTS) /* set keyMode to RTS */
         {
            fprintf (stderr, "Error cw_init: set to RTS failed\n");
            return -4;
         }

         #if DEBUG == 1
         fprintf (stderr, "cw_init - to RTS mode %s\n", serdevice);
         #endif

         break;

      default:   /* invalid mode */
         fprintf (stderr, "cw_init error: invalid mode %d\n", newmode);
         return -5;
   }

   /* if initialize == 0, then setup the codetable array */
   /* clear the array first, so unused chars become spaces */
   if (initialized == 0)
   {

      for (i = 1; i < MAX_LETTERS; i++)
      {
         codetable[i] = " ";
      }

      /* Pro Signs and Special */        /* Shift/Alt  */
      codetable[1]         = "........"; /* F1 = Error */
      codetable[2]         = ".-.-.";    /* F2 = <AR>  */
      codetable[3]         = "-.--.";    /* F3 = <KN>  */
      codetable[4]         = "...-.-";   /* F4 = <SK>  */
      codetable[5]         = ".-...";    /* F5 = <AS>  */
      codetable[6]         = "-...-.-";  /* F6 = <BK> */

      /* Numbers */
      codetable[(int) '1'] = ".----";
      codetable[(int) '2'] = "..---";
      codetable[(int) '3'] = "...--";
      codetable[(int) '4'] = "....-";
      codetable[(int) '5'] = ".....";
      codetable[(int) '6'] = "-....";
      codetable[(int) '7'] = "--...";
      codetable[(int) '8'] = "---..";
      codetable[(int) '9'] = "----.";
      codetable[(int) '0'] = "-----";
      codetable[216]       = "-----";   /* 216 or 330h is a zero with a slash */

      /* Alphabet */
      codetable[(int) ' '] = " ";
      codetable[(int) 'A'] = ".-";
      codetable[(int) 'B'] = "-...";
      codetable[(int) 'C'] = "-.-.";
      codetable[(int) 'D'] = "-..";
      codetable[(int) 'E'] = ".";
      codetable[(int) 'F'] = "..-.";
      codetable[(int) 'G'] = "--.";
      codetable[(int) 'H'] = "....";
      codetable[(int) 'I'] = "..";
      codetable[(int) 'J'] = ".---";
      codetable[(int) 'K'] = "-.-";
      codetable[(int) 'L'] = ".-..";
      codetable[(int) 'M'] = "--";
      codetable[(int) 'N'] = "-.";
      codetable[(int) 'O'] = "---";
      codetable[(int) 'P'] = ".--.";
      codetable[(int) 'Q'] = "--.-";
      codetable[(int) 'R'] = ".-.";
      codetable[(int) 'S'] = "...";
      codetable[(int) 'T'] = "-";
      codetable[(int) 'U'] = "..-";
      codetable[(int) 'V'] = "...-";
      codetable[(int) 'W'] = ".--";
      codetable[(int) 'X'] = "-..-";
      codetable[(int) 'Y'] = "-.--";
      codetable[(int) 'Z'] = "--..";

      /* Punctuation and special characters */
      codetable[(int) '='] = "-...-";
      codetable[(int) '/'] = "-..-.";
      codetable[(int) '?'] = "..--..";
      codetable[(int) ','] = "--..--";
      codetable[(int) '.'] = ".-.-.-";
      codetable[(int) ':'] = "---...";
      codetable[(int) '"'] = ".-..-.";

      codetable[(int) '('] = "-.--.";
      codetable[(int) ')'] = "-.--.-";
      codetable[(int) '['] = "-.--.";
      codetable[(int) ']'] = "-.--.-";

      codetable[(int) '-'] = "-....-";
      codetable[(int) ';'] = "-.-.-.";
      codetable[(int) '@'] = ".--.-.";
      codetable[(int) '+'] = ".-.-.";

      codetable[(int) '$'] = "...-..-";
      codetable[(int) '\''] = ".----.";       // single quote or apostrophe
      codetable[(int) '!'] = "-.-.--";
      codetable[(int) '&'] = ".-...";
      // codetable[(int) '%'] = "....-.--.";
      codetable[(int) '_'] = "..--.-";
      // codetable[(int) '['] = "....-..";
      // codetable[(int) ']'] = "....-....";
      // codetable[(int) '#'] = "-.---";
      // codetable[(int) '|'] = "...-..";
      // codetable[(int) '\\'] = "-.....";
      // codetable[(int) '*'] = "-----.";
      // codetable[(int) '^'] = "....--.-.";
      // codetable[(int) '>'] = "....---.";
      // codetable[(int) '<'] = "....-.-..";
      // codetable[(int) '~'] = "....--";
      // codetable[(int) '{'] = "....-.--";
      // codetable[(int) '}'] = "....--..-";

      /*
       * trap the following signals when exiting
       */
      signal (SIGINT,  (void *)cw_exit);
      signal (SIGTERM, (void *)cw_exit);
      signal (SIGQUIT, (void *)cw_exit);
      signal (SIGSEGV, (void *)cw_exit);

      initialized = 1;
   }
   return 0;
}


/*
 * cw_exit
 * Make sure the sidetone is off and the key is up, then exit
 */
void cw_exit (void)
{
   signal (SIGINT,  SIG_IGN);
   signal (SIGTERM, SIG_IGN);
   signal (SIGQUIT, SIG_IGN);
   signal (SIGSEGV, SIG_IGN);
   fprintf(stderr, "libCW terminating\n");

   if (ser_fd != 0)
   {
      if (ioctl (ser_fd, TIOCMSET, &rts_off) == -1)
      {
         perror("cw_exit: RTS key up failed");
      }
   }

   /* close pulseaudio */
   if (sp != NULL)
   {
//      pa_simple_free (sp);
   }
   exit (73);
}


/*
 * close_devs - making sure the key is up, then close ser_fd after 
 */
int close_devs (void)
{

   if (ser_fd != 0)                      /* if ser_fd is opened */
   {
      /* make sure the key is up */
      if (ioctl (ser_fd, TIOCMSET, &rts_off) < 0)
      {
         perror ("close_devs: RTS key up failed");
         return (0);
      }

      #if 0
      /*
       * USB to Serial dongles can take too much time to open and
       * close for every word.  Characters are right, but extra
       * time to open/close the dongle adds time to the inter letter
       * and inter word time.
       */
      close (ser_fd);                    /* close serial device */
      ser_fd = 0;
      #endif
   }
   return (1);
}


/*
 * cw_send_str
 * Called by all other cw_send_* functions.
 */
int cw_send_str (char *str)
{
   int rtn;
   static char s[MAX_WORD_LENGTH];

   /* cw_init must be called first */
   if (initialized == 0)
   {
      fprintf (stderr, "cw_send_str: cw_init must be called first.\n");
      return 0;
   }

   send_cw (str);
   return 1;
}


/*
 * cw_send_word
 * adds a space to the end of a word
 * to make it compatable, then calls cw_send_str.
 */
int cw_send_word (char str[])
{
   char new_str[MAX_WORD_LENGTH + 1];

   strcpy (new_str, str);

   if (str[strlen (str)] != ' ');    /* if word does not end with a space */
   {
      strcat (new_str, " ");
   }

   cw_send_str (new_str);
   return (1);
}


/*
 * cw_send_char - sends one char
 */
int cw_send_char (char ch)
{
   char str[2];

   str[0] = ch;
   str[1] = '\0';

   cw_send_str (str);
   return (1);
}


/*
 * send_cw
 * started by cw_send_str() or cw_send_word()
 */
void *send_cw (void *p)
{
   int i=0;
   char *pt;
   char *code;

   struct tag1                        /* must match the one in twcw.h */
   {
      long type;
      union
      {
         char word[MAX_WORD_LENGTH];
         int  val;
         float times[4];
      } data;
   } reply;

   if (strlen ((char *)p) == 0)     /* nothing to send */
   {
      return (0);
   }

   /* open the serial device if needed */
   if (keyMode == RTS)
   {
      ser_fd = open_serial ();
      if (ser_fd < 0)
      {
         fprintf (stderr, "send_cw: open %s failed\n", serdevice);
         return (0);
      }
   }

   pt = (char *) p;

   /* now run thru the string, and call keySC or keyTimer */
   while (pt[i] != '\0')
   {
      /* get the ./- representation for the character */
      code = codetable[(int)toupper((int)pt[i])];

      /* key devices */
      if (keyMode == PA)
      {
         keySC (code);
      }

      if (keyMode == RTS)
      {
         keyRTS(code);
      }

      if (msg_mode == 1)
      {
         reply.type = SENT_MSG;
         reply.data.word[0] = pt[i];
         reply.data.word[1] = '\0';
         if (msgsnd (qid, &reply, sizeof(reply), SENT_MSG) == -1)
         {
            perror ("send_cw: msgsnd SENT_MSG failed");
            exit (1);
         }
      }
      i++;
   }
   close_devs ();
   return (0);
}


/*
 * keySC (char *code)
 * uses the morse representation to key the SC
 */
void keySC (char *code)
{ 
   float t;
   int i = 0;

   while (TRUE)                          /* loop - breaks out when done */
   {   
      if (code[i] == ' ')                /* do a space or non-valid char */
      {                                  /* this assumes that the */
         tonegen (0, iw_time - il_time); /* previous character has */
         return;                         /* done the il_time already */
      }

      if (code[i] == DIT)
      {
         t = dit_time;
      }
      else if (code[i] == DAH)
      {
         t = dah_time;
      }
      else
      {
         fprintf (stderr, "keySC: Program error - not a DIT or DAH\n");
         exit (1);
      }

      tonegen (tone, t);                 /* and send the dit/dah */

      i++;                               /* ready for next character */
      if (code[i] == '\0')               /* end of character? */
      {
         tonegen (0, il_time);           /* done with this character */
         return;                         /* so do il_time and return */
      }
      else
      {
         tonegen (0, ic_time);           /* more to go, so do ic_time */
         continue;                       /* and go get the next ./- */
      }
   }
   return;
}


/*
 * keyRTS (char code)
 * uses the morse representation to key the RTS
 */
void keyRTS (char *code)
{ 
   int i = 0;
   float t;

   if (ser_fd == 0)  /* if no serial device */
   {
      return;
   }

   while (code[i] != '\0')
   {   
      if (code[i] == ' ')                      /* do a space or non-valid ch */
      {
         timer (iw_time - il_time);
         return;
      }

      if (code[i] == DIT)
      {
         t = dit_time;
      }
      else
      {
         t = dah_time;
      }

      if (ioctl (ser_fd, TIOCMSET, &rts_on) < 0)    /* key down */
      {
         perror("keyRTS: RTS key down failed");
      }

      timer (t);

      if (ioctl (ser_fd, TIOCMSET, &rts_off) < 0)   /* RTS key up */
      {
         perror ("keyRTS: RTS key up failed");
      }
      i++;
      if (strlen (code) > i)                   /* if more ./- to do */
      {                                        /* for this character */ 
         timer (ic_time);
      }
   }
   timer (il_time);
}


/*
 * tonegen
 * This function was mostly taken from qrq.
 * Copyright (C) 2006  Fabian Kurz, DJ1YFK.  Thanks, Fabian.
 * Generates a sinus tone of frequency tone  (Hz)of length len (ms)
 * based on 'SAMPLERATE', 'rt' (rise time), 'ft' (fall time).
 * len has 'weight' time included. buf array size is calculated
 * for the worse/longest case. Uses 7 dits when only 4 are needed.
 */
int tonegen (int tone, float len)
{
   int   x, rtn;
   int   *buf;               /* int pointer to sound buffer */
   float len_samps;
   float val = 0.0;
   float t;
   int   error;
   int   N = rt + ft;


   /* convert len from milliseconds to samples */
   len_samps = len * SAMPLERATE / 1000.0;

   /* malloc space for the sound buffer */
   buf = (int *) malloc ((len_samps + 1.0) * sizeof (int));

   #if DEBUG == 1
   printf ("len = %3.1f ms len_samps = %f\n", len, len_samps);
   #endif

   for (x=0; x < len_samps; x++)
   {

      /* if tone == 0, then quietly fill the buffer with zeros */
      if (tone == 0)                       /* quiet time */
      {
         buf[x] = 0;
      }
      else
      {
         /* generate sin wave */
         val = sin (omega * x/SAMPLERATE);

         /*
          * RISE TIME METHODS
          */
         if (x < rt)                       /* if within rise time */
         {
            switch (shape)
            {
               /* Hann/Raised Cosine window */
               case HANNING:
                  val = val * 0.5 * (1-cos(PI*x/rt));
                  break;

               /* for Nuttall and all Blackman style windows */
               case NUTTALL:
               case BLACKMAN:
               case BLACK_NUT:
               case BLACK_HAR:
               case HAMMING:
#if PLAYTIME == 1
               case PLAY:
#endif

                  /* all of the above gets the Hann shape first */
                  val = val * 0.5 * (1-cos(PI*x/rt));

                  /* now use selected window */
                  val = val * (a0 - a1 * cos(2.0*PI*x/(N-1))
                            +  a2 * cos (4*PI*x/(N-1))
                            -  a3 * cos (6*PI*x/(N-1)));
                  break;

               /* RC time constant */
               case RC:
                  t = (float)x/(float)SAMPLERATE;
                  val = val * (1.0 - exp (-t/tau_r));
                 break;

               default:
                  fprintf (stderr, "tonegen: invalid rise shape\n");
            }
         }

         /*
          * FALL TIME METHODS
          */
         if (x > (len_samps-ft))                /* if within fall time */
         { 
            switch (shape)
            {
               /* Hanning/raised cos window */
               case HANNING:
                  val = val * 0.5 * (1-cos(PI*(len_samps-x)/ft));
                  break;

               /* for Nuttall and all Blackman style windows */
               case NUTTALL:
               case BLACKMAN:
               case BLACK_NUT:
               case BLACK_HAR:
               case HAMMING:
#if PLAYTIME == 1
               case PLAY:
#endif

                  /* all of the above  gets the Hann shape first */
                  val = val * 0.5 * (1-cos(PI*(len_samps-x)/ft));

                  /* now use selected window */
                  val = val * (a0 - a1*cos(2.0*PI*(len_samps-x)/(N-1)) 
                            + a2 * cos(4*PI*(len_samps-x)/(N-1))
                            - a3 * cos(6*PI*(len_samps-x)/(N-1)));
                  break;

               case RC:
                  /* RC time constant window */
                  //t = fall/1000.0 - ((len_samps-x)/SAMPLERATE);
                  //val = val * exp (-t/tau_f);

                  /* or this for symmetry with the rise time */
                  t = (float)(len_samps-x) / (float)SAMPLERATE; 
                  val = val * (1 - exp (-t/tau_f));
                  break;

               default:
                  fprintf (stderr, "tonegen - invalid fall shape\n");
            }
         }

         /* fill the buffer - max is 80% of max short */
         buf[x] = SHRT_MAX * val * 0.8;          /* into the buffer */
         buf[x] += (buf[x]<<(8*sizeof(short)));  /* add channel 2 */
      }
   }

   /* now write the buffer to the soundcard */
   rtn = pa_simple_write (sp, buf, sizeof (int) *x, &error); 
   if (rtn == -1)
   {
      fprintf (stderr, "pa_simple_write failed - error=%d: %s\n",
               error, pa_strerror(error));
   }
   free (buf);
   return rtn;
}


/*
 * timer - use the real time clock for accurate timing
 */
int timer (float msec)
{
   unsigned long rtc_data;
   int fd;
   int i;
   int cnt;

   cnt = msec * rtc_rate / 1000.0;
   /* open rtc */
   {
      if ((fd = open ("/dev/rtc", O_RDONLY)) < 0)
      {
         perror ("timer: open /dev/rtc failed");
         return -1;
      }
   }

   /* configure rtc for periodic interupts */
   if (ioctl (fd, RTC_IRQP_SET, rtc_rate) < 0)
   {
      perror ("timer: RTC_IRQP_SET");
      return -1;
   }

   if (ioctl (fd, RTC_PIE_ON, 0) < 0)
   {
      perror ("timer: PIE_ON");
      return -1;
   }

   i = 0;
   while (cnt - i > 0)
   {
      if (read (fd, &rtc_data, sizeof(rtc_data)) < 0)
      {
         perror ("timer: RTC read");
         exit (1);
      }
      i = i + (rtc_data >> 8);
   }

   if (ioctl (fd, RTC_PIE_OFF, 0) < 0)
   {
      perror ("timer: PIE_OFF");
      exit (1);
   }
   close (fd);
   return 0;
}


/*
 * cw_get_doReply - returns the current msg mode
 */
int cw_get_doReply (void)
{
   return msg_mode;
}


/*
 * cw_set_doReply - sets new msg_mode to mode - 0=no 1=yes
 */
int cw_set_doReply (int mode)
{
   msg_mode = mode;
   return 1;
}


/*
 * cw_get_serdev - returns the current key mode
 */
char *cw_get_serdev (char *p)
{
      return (serdevice);
}


/*
 * cw_set_serdev - returns the current key mode
 */
void cw_set_serdev (char *ser)
{
   strcpy (serdevice, ser);
}


/*
 * cw_get_keyMode
 */
int cw_get_keyMode (void)
{
   return (keyMode);
}


/*
 * cw_set_keyMode (int newmode, char *str)
 * Use PA or RTS for newmode.
 * str is serdevice for RTS or name for PA volume control for PA
 * Returns -1 if PA key mode and pa_name is not set
 * Returns -2 if RTS key mode and serdevice is not set
 * Returns newmode if it works.
 */
int cw_set_keyMode (int newmode, char *str)
{  
   int fd, error, rtn = 0;

   /* setup the key mode */
   switch (newmode)
   {
      case PA:
         /* if name not passed and pa_name has not been set */
         if ((strlen (str) == 0) && (strlen (pa_name) == 0))
         {
            fprintf (stderr, "Warning cw_set_keyMode - No program name "
                     "for PA Volume Control\n");
            return (-1);
         }

         #if DEBUG == 1
         fprintf (stderr,
                  "set_keyMode PA "
                  "wpm=%2.0f/%2.0f dit=%4.1f dah=%4.1f\n"
                  "il=%4.1f iw=%4.1f rise=%2.1f fall=%2.1f weight=%2.1f "
                  "pa_name=%s\n",
                  wpm, farns_wpm, dit_time, dah_time,
                  il_time, iw_time, rise, fall, weight,
                  pa_name);
         #endif
         break;

      case RTS:
         /* if name not passed and serdevice is not set */
         if ((strlen (str) == 0) && (strlen (serdevice) == 0))
         {
            fprintf (stderr, "Warning: cw_set_keyMode - "
                     "RTS mode needs serial device\n");
            rtn = -2;
         }

         if (strlen (str) != 0)
         {
            strncpy (serdevice, str, MAX_NAME - 1); /* save str as serdevice */ 
         }

         /* open rtc and find the highest interupt rate */
         if ((fd = open ("/dev/rtc", O_RDONLY)) < 0)
         {
            perror ("cw_set_keyMode: open /dev/rtc failed");
            exit (1);
         }
         /* find the highest interupt rate */
         /* if 64 is the system default rate, that's kinda slow */
         rtc_rate = MAX_USER_FREQ;
         while (ioctl(fd, RTC_IRQP_SET, rtc_rate) < 0)
         {
            fprintf (stderr, "cw_set_keyMode RTC_IRQP_SET %d failed\n",
                     rtc_rate);
            rtc_rate = rtc_rate - 64;
         }
         fprintf (stderr, "cw_set_keyMode - RTC SUCCESS rate = %d\n", rtc_rate);
         close (fd);   /* close the rtc */

         #if DEBUG == 1
         fprintf (stderr,
                  "set_keyMode RTS wpm=%2.0f/%2.0f dit=%4.1f dah=%4.1f\n"
                  "il=%4.1f iw=%4.1f rise=%2.1f fall=%2.1f weight=%2.1f "
                  "serdevice=%s\n",
                  wpm, farns_wpm, dit_time, dah_time,
                  il_time, iw_time, rise, fall, weight, serdevice);
         #endif
         break;

      default:
         fprintf (stderr, "cw_set_keyMode: invalid mode %d\n", newmode);
         return (keyMode);     /* return old keyMode */
   }
   keyMode = newmode;
   return keyMode;
}


/*
 * cw_get_tone - gets the tone of the audio out
 */
int cw_get_tone (void)
{
   return tone;
}


/*
 * cw_set_tone - sets the tone of the audio out
 */
int cw_set_tone (int new_tone)
{
   tone = new_tone;
   omega = 2.0 * PI * tone;            /* calc new omega */
   return tone;
}


/* 
 * cw_get_wpm - returns current wpm
 */
int cw_get_wpm (void)
{
   return (wpm);
}


/*
 * cw_set_wpm
 * set wpm.  Calculate the times for all character, including the
 * iw_time and ic_time for farnsworths, if required. Also figure the
 * rise/fall times
 */
int cw_set_wpm (int new_wpm)
{
   /* cw_init must be called first */
   if (initialized == 1)
   {
      return calc_wpm (new_wpm);            /* call calc_wpm to change values */
   }
}


/*
 * calc_wpm - given a new wpm, calculate the times and sample 
 * sizes for all of the morse elements. dit, dah, etc.
 * set wpm, farns and weight. Returns wpm.
 */
int calc_wpm (int new_wpm)
{
   float base_time;
   float wpm_time;
 
   /*
    * speed is in wpm so we have to calculate the dot length in
    * milliseconds using the well known formula -> dotlength = 1200 / wpm
    */
   wpm = new_wpm;
   base_time = 1200.0 / wpm;                   /* true dit time */

   /* adjust for ratio and weight time */
   dit_time = (base_time * dit_factor) + weight;
   dah_time = (3.0 * base_time * dah_factor) + weight;

   ic_time = base_time - weight;               /* intra character time */
   il_time = (3.0 * base_time) - weight;       /* inter letter time */

   iw_time = (7.0 * base_time) - weight;       /* inter word time */

   /* if needed, do farnsworth timing */
   if ((wpm > farns_wpm) && (farns_wpm > farns_off))
   { 
      farns_time = 1200.0 / farns_wpm * 50.0;  /* time for a "word" @ fwpm */ 
      wpm_time = 31.0 * dit_time;              /* PARIS minus iw_time @ wpm */
      il_time = 3.0/19.0 * (farns_time - wpm_time);
      iw_time = 7.0/19.0 * (farns_time - wpm_time);
   }

   #if DEBUG == 1
   fprintf (stderr, "calc_wpm wpm=%2.0f/%2.0f dit=%4.3f dah=%4.3f\n"
            "ic=%4.2f il=%4.2f iw=%4.2f rt=%2.1f ft=%2.1f wt=%2.1f\n"
            "ratio=%2.1f\n\n",
            wpm, farns_wpm, dit_time, dah_time,
            ic_time, il_time, iw_time, rise, fall, weight, ratio);
   #endif
   return wpm;
}


/* 
 * cw_get_fwpm - returns current farnsworth wpm
 */
int cw_get_fwpm (void)
{
   return (farns_wpm);
}


/*
 * cw_set_fwpm
 * set farns_wpm.  Calculate the farnsworth times for il_time and
 * iw_time for farnsworth.
 */
int cw_set_fwpm (int new_fwpm)
{
   if (initialized == 0)
   {
      fprintf (stderr, "cw_set_fwpm: cw_init must be called first.\n");
      return -1;
   }
   farns_wpm = new_fwpm;          /* set new fwpm and call */
   return calc_wpm (wpm);         /* call calc_wpm to change values */ 
}


/*
 * cw_get_farns_off
 * returns the current farnsworth wpm off speed
 * default farns_off is 7 wpm
 */
int cw_get_farns_off (void)
{
   return farns_off;
}


/*
 * cw_set_farns_off wpm
 * sets new farnsworth wpm off speed
 * no farnsworth when wpm is <= farns_off
 * or when farns_wpm >= wpm
 * default farns_off is 7 wpm
 */
int cw_set_farns_off (int new_farns_off)
{
   farns_off = new_farns_off;
   calc_wpm (wpm);                /* call calc_wpm to change values */ 
   return farns_off;
}


/*
 * open_serial
 * open the serial device and return fd. 
 */
int open_serial ()
{
   int fd = 0;

   if (ser_fd != 0)         /* already open? */
   {
      /* yes, so just make sure the key is up and return ser_fd */
      ioctl (fd, TIOCMSET, &rts_off);
      return (ser_fd);
   }

   if ((fd = open(serdevice, O_RDWR, 0)) < 0)
   {
      perror("open_serial: can't open the serial device");
   }
   else
   {
      /* make sure the key is up */
      if (fd > 0)
      {
         ioctl (fd, TIOCMSET, &rts_off);
      }
   }
   return fd;   /* return fd or error */
}


/*
 * cw_get_rise_time - returns current rise time in msecs
 */
float cw_get_rise_time (void)
{
   return (rise);
}


/*
 * cw_set_rise_time - arg is rise time in msecs
 * save rise time and convert to samples in rt.  
 */
float cw_set_rise_time (float r)
{
   rise = r;                        /* save rise in msec */
   rt = r / 1000 * SAMPLERATE;      /* convert rise to samples */
   return rt;
}


/*
 * cw_get_fall_time - returns current fall time in msecs
 */
float cw_get_fall_time (void)
{
   return (fall);
}


/*
 * cw_set_fall_time - arg is fall time in msecs
 * save fall time and convert to samples in ft.
 */
float cw_set_fall_time (float f)
{
   fall = f;                        /* save fall in msec */
   ft = f / 1000 * SAMPLERATE;      /* convert fall to samples */
   return ft;
}


/*
 * cw_get_weight
 */
float cw_get_weight ()
{
   return (wt);
} 


/*
 * cw_set_weight
 */
float cw_set_weight (float w)
{
   weight = w;                          /* save weight in msec */
   wt = w / 1000 * SAMPLERATE;          /* convert weight to samples */
   calc_wpm (wpm);                      /* call calc_wpm to change values */
}


/*
 * cw_get_ratio
 */
float cw_get_ratio ()
{
   return (ratio);
} 


/*
 * cw_set_ratio
 */
float cw_set_ratio (float w)
{
   ratio = w;                                   /* save ratio in ms */

   dit_factor = 1.0 + ratio / 100.0;            /* +% increases dit time */
   dah_factor = 1.0 - (ratio / 100.0) / 3.0;    /* and decreases dah time */

   #if DEBUG == 1
   fprintf (stdout, "ratio val %2.2f dit_factor=%1.3f dah_factor=%1.3f\n",
            ratio, dit_factor, dah_factor);
   #endif

   calc_wpm (wpm);                              /* call calc to change values */
   return ratio;
}


/*
 * cw_get_shape
 */
int cw_get_shape ()
{
   return (shape);
} 


/*
* cw_set_shape
 */
int cw_set_shape (int new_shape)
{
   /* Nuttall values */
   float n0 = 0.355768;
   float n1 = 0.487396;
   float n2 = 0.144232;
   float n3 = 0.012604;

   /* Blackman */
   float b0 = 0.42659;
   float b1 = 0.49656;
   float b2 = 0.076849;
   float b3 = 0.0;

   /* Blackman-Nuttall */
   float bn0 = 0.3635819;
   float bn1 = 0.4891775;
   float bn2 = 0.1365995;
   float bn3 = 0.0106411;

   /* Blackman-Harris */
   float bh0 = 0.35875;
   float bh1 = 0.48829;
   float bh2 = 0.14128;
   float bh3 = 0.01168;

   /* Hamming */
   float h0 = 0.53836;
   float h1 = 1.0 - h0;     /* h2 = h3 = 0  set below */

   shape = new_shape; 

   switch (shape)
   {
      case NUTTALL:
         a0 = n0;
         a1 = n1;
         a2 = n2;
         a3 = n3;
         break;

      case BLACKMAN:
         a0 = b0;
         a1 = b1;
         a2 = b2;
         a3 = b3;
         break;

      case BLACK_NUT:
         a0 = bn0;
         a1 = bn1;
         a2 = bn2;
         a3 = bn3;
         break;

      case BLACK_HAR:
         a0 = bh0;
         a1 = bh1;
         a2 = bh2;
         a3 = bh3;
         break;

      case HAMMING:
         a0 = h0;
         a1 = h1;
         a2 = 0.0;
         a3 = 0.0;
         break;

#if PLAYTIME == 1
extern a0Scale, a1Scale, a2Scale, a3Scale;
int val;

      case PLAY:
         XmScaleGetValue (a0Scale, &val);
         a0 = val/10000.0;
         XmScaleGetValue (a1Scale, &val);
         a1 = val/10000.0;
         XmScaleGetValue (a2Scale, &val);
         a2 = val/10000.0;
         XmScaleGetValue (a3Scale, &val);
         a3 = val/10000.0;
         //TJW printf ("%f %f %f %f\n", a0, a1, a2, a3)
         break;
#endif

      default:
         break;               /* RC and Hann/raised cos comes here */
   }

#if PLAYTIME == 1
extern toyFrame;
if (shape == PLAY)
{
   XtManageChild (toyFrame);
}
#endif
   return (shape);
} 


#if TESTMODE == 1
/*
 * main function for testing standalone
 */
int main (int argc, char *argv[])
{
   int i, j, rtn;
   char d;
   char pgmName[]  = "libCW";
   char newdev[] = "/dev/ttyUSB0";

   /*
    * select one key mode
    */
   //int mode = RTS;
   int mode = PA;

   /* initialize libCW */
   rtn = cw_init (mode, "libCW", newdev);
   if (rtn < 0)
   {
      printf ("cw_init failed %d\n", rtn);
      exit (1);
   }

   /* set some other setup functions */
   cw_set_tone (1000);
   //cw_set_wpm (20);                      /* set wpm */
   //cw_set_fwpm (10);                     /* set fwpm */
   //cw_set_rise_time (2.0);

   #if DEBUG == 1
   printf ("rise time is %2.2f ms %f samp\n", cw_get_rise_time (),rt);
   printf ("fall time is %2.2f ms %f samp\n", cw_get_fall_time (), ft);
   #endif

   printf ("Enter to start: ");          /* hit rtn when ready */
   d = getchar ();

   /*
    * add what ever other ways you want to test libCW here
    */

   /*
    * send paris (as chars) 20 times
    */
   #if 1
   printf ("Send chars\n");
   for (i=0; i<20; i++)
   {
      cw_send_char ('p');
      cw_send_char ('a');
      cw_send_char ('r');
      cw_send_char ('i');
      cw_send_char ('s');
      cw_send_char (' ');
   }
   #endif

   /*
    * send paris 20 times as strings - string includes spaces
    * twcw sends data this way
    */
   #if 0
   printf ("Send string\n");
   for (i=0; i<20; i++)
   {
       cw_send_str ("paris");
   }
   #endif

   /*
    * send paris 20 times as words - appends a space (not for twcw)
    */
   #if 0
   printf ("Send word\n");
   for (i=0; i<20; i++)
   {
      cw_send_word ("paris");
   }
   #endif

   /*
    * send my call
    */
   #if 0
   printf ("send my call\n");
   cw_send_str ("de wa0eir\n");
   #endif

   /*
    * send paris 20 times as split strings - string includes spaces
    */
   #if 0
   printf ("Send split strings\n");
   for (i=0; i<4; i++)
   {
      cw_send_str ("paris");
      cw_send_str ("paris");
      cw_send_str ("paris");
      cw_send_str ("paris");
      cw_send_str ("paris\n");
   }
   #endif
   //cw_send_str ("e ");

   sleep (1);
   cw_exit ();
   return 0;
}
#endif   /* END TESTMODE */

