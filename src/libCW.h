/* 
 * libCW - Copyright (C) 2006-2018 by Ted Williams - WA0EIR
 * Derived mostly from qrq.c - High speed morse trainer
 * by Fabian Kurz, DJ1YFK, Copyright (C) 2006, and
 * a few bits from unixcw
 * by Simon Baldwin, G0FRD, Copyright (C) 2001-2006
 * 
 * This file is part of libCW
 *
 * libCW is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>
 */

/*
 * libCW.h
 */
#define RTS 0
#define PA  1

#define DEFAULT_WPM        20        /* default wpm */
#define DEFAULT_FARNS_OFF   7        /* default min farns wpm */
#define DEFAULT_RT          4.0      /* default rise time in msec */
#define DEFAULT_FT          4.0      /* default fall time in msec */
#define DEFAULT_WT          0.0      /* default weight time in msec */
#define DEFAULT_RATIO       0.0      /* default ratio */
#define DEFAULT_TONE      800        /* default audio tone */
#define DEFAULT_SHAPE       0        /* default shape is Hanning */

#define PLAYTIME 0

/* Shape Modes */
#define HANNING   0
#define NUTTALL   1
#define BLACKMAN  2
#define BLACK_NUT 3
#define BLACK_HAR 4
#define HAMMING   5
#define RC        6
#define PLAY      7

int   cw_init (int newmode, char *name, char *ser);

int   cw_get_keyMode (void);
int   cw_set_keyMode (int newmode, char *str);

char  *cw_get_serdev ();
void  cw_set_serdev (char *serdev);

int   cw_get_doReply (void);
int   cw_set_doReply (int mode);

int   cw_get_wpm (void);
int   cw_set_wpm (int new_wpm);

int   cw_get_fwpm (void);
int   cw_set_fwpm (int new_fwpm);

int   cw_get_farns_off (void);
int   cw_set_farns_off (int new_farns_off);

float cw_get_rise_time (void);
float cw_set_rise_time (float r);

float cw_get_fall_time (void);
float cw_set_fall_time (float f);

float cw_get_weight (void);
float cw_set_weight (float w);

float cw_get_ratio (void);
float cw_set_ratio (float ratio);

int   cw_get_shape (void);
int   cw_set_shape (int shape);

int   cw_get_tone (void);
int   cw_set_tone (int new_tone);

int   cw_send_char (char ch);
int   cw_send_str  (char *str);
int   cw_send_word (char *str);

void  cw_exit (void);

/************** end sendCW.h **************/
