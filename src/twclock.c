/*
 * twclock:  A world clock implemented with openMotif widgets
 * Copyright (C) 1997-2020 Ted Williams - WA0EIR
 *
 * This file is part of twclock
 *
 * twclock is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have recieved a copy of the GNU General Public License
 * along with this program. If not, see <http://gnu.org/licences/>.
 *
 * Versions: 3.5 -  Jan 2020
 */

/*
 * twclock.c
 */
#include "twclock.h"

#if (MAKE_ICON == 1)  && (HAVE_X11_XPM_H == 1) && (HAVE_LIBXPM == 1)
   #include <X11/xpm.h>
   #include "icons/twclock.xpm"
   Pixmap pixmap, mask;
   XpmAttributes pix_attributes;
   XpmColorSymbol transparentColor[1] = {{NULL, "none", 0}};
#endif

/*
 * Globals
 */
XtAppContext app;
AppRes appRes;

int    post_from;
Widget clock_shell;
Widget call_toggleB;
Widget hr_scale;
Widget min_scale;
Widget sec_scale;
Widget date_label;

int doit;
int count;
int flashFlag;
int cwIDdone;
XtIntervalId alarm_id;
XtIntervalId auto_id;
XtIntervalId time_id;


int main (int argc, char *argv[])
{
   XtResource appRes_desc[] =
   {
      {
      XmNtzone,                      /* Name */
      XmCTzone,                      /* Class */
      XmRInt,                        /* Target data type */
      sizeof (int),                  /* size of target type */
      XtOffsetOf (AppRes, tzone),    /* Offset into struct */
      XtRImmediate,                  /* Default data type */
      (XtPointer) -1                 /* Defaults to -1 if Twclock not found */
      },

      {
      XmNblink,                      /* Same as above */
      XmCBlink,
      XmRInt,
      sizeof (int),
      XtOffsetOf (AppRes, blink),
      XtRImmediate,
      (XtPointer) 1
      },

      {
      XmNbeep,                       /* Same as above */
      XmCBeep,
      XmRInt,
      sizeof (int),
      XtOffsetOf (AppRes, beep),
      XtRImmediate,
      (XtPointer) 1
      },

      {
      XmNcwID,                       /* Same as above */
      XmCCwID,
      XmRInt,
      sizeof (int),
      XtOffsetOf (AppRes, cwID),
      XtRImmediate,
      (XtPointer) 0
      },

      {
      XmNcwStr,                      /* Same as above */
      XmCCwStr,
      XtRString,
      sizeof (String),
      XtOffsetOf (AppRes, cwStr),
      XtRImmediate,
      (XtPointer) "n0call"
      },

      {
      XmNkeyMode,                    /* Same as above */
      XmCKeyMode,
      XmRInt,
      sizeof (int),
      XtOffsetOf (AppRes, keyMode),
      XtRImmediate,
      (XtPointer) 0                  /* default to pulseaudio */
      },

      {
      XmNcwSpeed,                    /* Same as above */
      XmCCwSpeed,
      XtRInt,
      sizeof (int),
      XtOffsetOf (AppRes, cwSpeed),
      XtRImmediate,
      (XtPointer) 20
      },

      {
      XmNcwTone,                     /* Same as above */
      XmCCwTone,
      XtRInt,
      sizeof (int),
      XtOffsetOf (AppRes, cwTone),
      XtRImmediate,
      (XtPointer) 450
      },

      {
      XmNautoReset,                  /* Same as above */
      XmCAutoReset,
      XmRInt,
      sizeof (int),
      XtOffsetOf (AppRes, autoReset),
      XtRImmediate,
      (XtPointer) 0
      },

      {
      XmNautobeeps,                  /* Same as above */
      XmCAutobeeps,
      XmRInt,
      sizeof (int),
      XtOffsetOf (AppRes, autobeeps),
      XtRImmediate,
      (XtPointer) 5
      },

      {
      XmNflashColor,                 /* Same as above */
      XmCFlashColor,
      XmRPixel,
      sizeof (Pixel),
      XtOffsetOf (AppRes, flashColor),
      XtRImmediate,
      (XtPointer) 1
      },

      {
      XmNminutes,                    /* Same as above */
      XmCMinutes,
      XmRInt,
      sizeof (int),
      XtOffsetOf (AppRes, minutes),
      XtRImmediate,
      (XtPointer) 10
      },

      {
      XmNseconds,                    /* Same as above */
      XmCSeconds,
      XmRInt,
      sizeof (int),
      XtOffsetOf (AppRes, seconds),
      XtRImmediate,
      (XtPointer) 0
      },
  };

#ifdef XmVERSION_STRING
   extern const char _XmVersionString[];
   fprintf (stderr, "Compiled with %s\n", XmVERSION_STRING + 4);
   fprintf (stderr, "Running  with %s\n", _XmVersionString + 4);

   /* LessTif and Motif are counting buttons in popups differently
    * Needs 10 for Motif and 5 for LessTif
    */
   if (strncmp(_XmVersionString +  4, "M", 1) == 0)
      post_from = 10;
   else
      post_from = 5;
#endif

   /*
    * Create clockShell and set some of its properties
    */
   clock_shell = XtVaOpenApplication (&app, "Twclock",
      NULL, 0, &argc, argv, NULL,
      sessionShellWidgetClass, 
      XmNtitle, PACKAGE_STRING,
      XmNminHeight, 175,
      XmNminWidth, 175,
      XmNmaxHeight, 250,
      XmNmaxWidth, 250,
      XmNminAspectX, 1,
      XmNminAspectY, 1,
      XmNmaxAspectX, 1,
      XmNmaxAspectY, 1,
      NULL);

   XtGetApplicationResources (clock_shell, &appRes, appRes_desc,
      XtNumber (appRes_desc), NULL, 0);

   /*
    * Check tzone to see if we are running on the default value (-1).
    * If so, Twclock was not found and things won't look right.
    */
   if (appRes.tzone == -1)
   {
      fprintf (stderr, "twclock: The resource file, Twclock, not found\n");
      XtRealizeWidget (clock_shell);   
      errorDiag (clock_shell, "The resource file, Twclock, is missing.\n"
                              "This is an installation problem.  Did you\n"
                              "install twclock as root?  twclock must exit.\n\n"
                              "Please send bug reports to\n" PACKAGE_BUGREPORT,
                               CANCEL_BTN);
      appRes.tzone = 1;               /* set time zone to GMT. or quit? */
   }

   /* Initialize libCW */
// TJW printf ("key=%d speed=%d\n", appRes.keyMode, appRes.cwSpeed);
   cw_init (appRes.keyMode, "TWCLOCK", "dummy");
   cw_set_wpm (appRes.cwSpeed);
// TJW printf ("speed %d\n", appRes.cwSpeed);

#if (MAKE_ICON == 1) && (HAVE_X11_XPM_H == 1) && (HAVE_LIBXPM == 1)
   /*
    *  then create pixmap for application Icon
    */
   pix_attributes.closeness = 40000;
   pix_attributes.valuemask = XpmColorSymbols | XpmCloseness;
   pix_attributes.colorsymbols = transparentColor;
   pix_attributes.numsymbols = 1;
   
   XpmCreatePixmapFromData (XtDisplay(clock_shell),
      DefaultRootWindow (XtDisplay (clock_shell)),
      twclock_xpm, &pixmap, &mask, &pix_attributes);

   XtVaSetValues (clock_shell,
      XmNiconPixmap, pixmap,
      NULL);
#endif

   /*
    * Build and Realize the rest of the widget tree
    */
   build_widgets();
   XtRealizeWidget (clock_shell);   

   /*
    * Enter the event loop
    */
   XtAppMainLoop (app);
   return (0);
}






