/*
 * twclock:  A world clock implemented with openMotif widgets
 * Copyright (C) 1997-2020 Ted Williams - WA0EIR
 *
 * This file is part of twclock
 *
 * twclock is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have recieved a copy of the GNU General Public License
 * along with this program. If not, see <http://gnu.org/licences/>.
 *
 * Versions: 3.5 -  Jan 2020
 */

/*
 * twclock.h
 */

#ifndef TWCLOCK_H_INCLUDED
#define TWCLOCK_H_INCLUDED

#include "libCW.h"
#include <time.h> 
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <pthread.h>

#include <Xm/Xm.h>
#include <Xm/Form.h>
#include <Xm/Text.h>
#include <Xm/PushB.h>
#include <Xm/TextF.h>
#include <Xm/Scale.h>
#include <Xm/Label.h>
#include <Xm/Frame.h>
#include <Xm/FileSB.h>
#include <Xm/DialogS.h>
#include <Xm/ToggleB.h>
#include <Xm/MwmUtil.h>
#include <Xm/MessageB.h>
#include <Xm/Separator.h>
#include <Xm/RowColumn.h>
#include <Xm/ScrolledW.h>
#include <Xm/Separator.h>
#include "../config.h"

#define MAKE_ICON  1

#define LOCAL_BTN  0
#define GMT_BTN    1
#define OTHERS_BTN 2
#define SET_TIMER_BTN 3
#define ID_NOW_BTN 4
#define HELP_BTN 5
#define QRT 6

#define ABOUT_DIAG  0
#define HELP_DIAG   1
#define CANCEL_BTN  1

/*
 * APPLICATION DEFINED RESOURCES
 */
#define XmNtzone           "tzone"
#define XmCTzone           "Tzone"

#define XmNblink           "blink"
#define XmCBlink           "Blink"

#define XmNbeep            "beep"
#define XmCBeep            "Beep"

#define XmNcwID            "cwID"
#define XmCCwID            "CwID"

#define XmNcwStr           "cwStr"
#define XmCCwStr           "CwStr"

#define XmNkeyMode         "keyMode"
#define XmCKeyMode         "KeyMode"

#define XmNcwSpeed         "cwSpeed"
#define XmCCwSpeed         "CwSpeed"

#define XmNcwTone          "cwTone"
#define XmCCwTone          "CwTone"

#define XmNautoReset       "autoReset"
#define XmCAutoReset       "AutoReset"

#define XmNautobeeps       "autobeeps"
#define XmCAutobeeps       "Autobeeps"

#define XmNflashColor      "flashColor"
#define XmCFlashColor      "FlashColor"

#define XmNminutes         "minutes"
#define XmCMinutes         "Minutes"

#define XmNseconds         "seconds"
#define XmCSeconds         "Seconds"

typedef struct
   {
      long    tzone;
      int     blink;
      int     beep;
      int     cwID;
      char    *cwStr;
		int     keyMode;
      int     cwSpeed;
      int     cwTone;
      int     autoReset;
      int     autobeeps;
      Pixel   flashColor;
      int     minutes;
      int     seconds;
   } AppRes;

/*
 * Forward Declarations
 */
extern XtAppContext app;
extern AppRes appRes;

extern int    post_from;
extern Widget clock_shell;
extern Widget call_toggleB;
extern Widget hr_scale;
extern Widget min_scale;
extern Widget sec_scale;
extern Widget date_label;

extern int doit;
extern int count;
extern int flashFlag;
extern int cwIDdone;
extern XtIntervalId alarm_id;
extern XtIntervalId auto_id;
extern XtIntervalId time_id;


void build_widgets (void);
void input_event (Widget, XtPointer, XEvent *, Boolean *);
void popup_cb (Widget, XtPointer, XtPointer);
void help_cas_cb (Widget w, XtPointer client_data, XtPointer cbs);
void popup_about (void);
void popup_help (void);
void TBchangeCB (Widget, XtPointer, XtPointer);
void optionsCB (Widget, XtPointer, XtPointer);
void scalesCB (Widget, XtPointer, XtPointer);
void alarm_timer (void);
void gettime (void);
void fsbOkCB (Widget , XtPointer, XtPointer);
void fsbCancelCB (Widget , XtPointer, XtPointer);
void timerOkCB (Widget , XtPointer, XtPointer);
void timerModVerCB (Widget , XtPointer, XtPointer);
void errorDiag (Widget, char *, int);
void errOkCB (Widget , XtPointer, XtPointer);
void errCancelCB (Widget , XtPointer, XtPointer);
void settimer (void);

#endif
