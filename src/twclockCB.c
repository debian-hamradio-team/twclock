/*
 * twclock:  A world clock implemented with openMotif widgets
 * Copyright (C) 1997-2020 Ted Williams - WA0EIR
 *
 * This file is part of twclock
 *
 * twclock is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have recieved a copy of the GNU General Public License
 * along with this program. If not, see <http://gnu.org/licences/>.
 *
 * Versions: 3.5 -  Jan 2020
 */

/*
 * CALLBACKS FOR TWCLOCK 
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include "twclock.h"

#if (MAKE_ICON == 1) && (HAVE_X11_XPM_H == 1) && (HAVE_LIBXPM == 1)
extern Pixmap pixmap;
#endif

Pixel saveColor;
Boolean newTimeFlag;
char timepath[100];

/*
 * CATCH INPUT EVENTS
 * This catches all input events in the GUI.  If it was a
 * button 3 press, then popup the menu, else go away.
 */
void input_event (Widget w, XtPointer client_data, XEvent *event, Boolean *cont)
{
   Widget popup_menu = (Widget) client_data;
   //if(event->type == ButtonPress && event->xbutton.button == Button3)
   if (event->xbutton.button != Button3)
      return;
   {
      XmMenuPosition(popup_menu, &(event->xbutton));
      XtManageChild(popup_menu); 
   }
}


/*
 * POPUP MENU CALLBACK
 * All popup menu button presses come here.  The switch checks for
 * which menu button was pressed and either sets the TZ variable 
 * to Local, GMT or, in the case of the Others: button, pops
 * up the file selection box dialog.  The clock will adjust
 * to the new time on the next update.
 */
void popup_cb(Widget w, XtPointer client_data, XtPointer cbs)
{
   long btn_num = (long) client_data;
   Widget zone_fsb;

   int ret;
   pthread_t thread;

   switch (btn_num)
   {
      case LOCAL_BTN:
         strcpy (timepath, "");
         doit = 1;     /* force the date label to be updated next time */
         break;

      case GMT_BTN:
         strcpy (timepath, "TZ=GMT");
         doit = 1;     /* force the date label to be updated next time */
         break;

      case OTHERS_BTN:
         zone_fsb = XmCreateFileSelectionDialog(clock_shell, "zone_fsb",
            (ArgList) NULL, 0);

         XtVaSetValues (XtParent(zone_fsb),
            XmNtitle, "TWCLOCK TIME ZONE SELECTION",
         #if (MAKE_ICON == 1) && (HAVE_X11_XPM_H == 1) && (HAVE_LIBXPM == 1)
            XmNiconPixmap, pixmap,
         #endif
            NULL);

         XtVaSetValues (zone_fsb,
            XmNdialogStyle, XmDIALOG_MODELESS,
            NULL);

         /* Unmanage some fsb widgets */
         XtUnmanageChild (XmFileSelectionBoxGetChild (zone_fsb,
            XmDIALOG_FILTER_TEXT));

         XtUnmanageChild (XmFileSelectionBoxGetChild (zone_fsb,
            XmDIALOG_FILTER_LABEL));

         XtUnmanageChild (XmFileSelectionBoxGetChild (zone_fsb,
            XmDIALOG_HELP_BUTTON));

         /* Add callbacks for file selection box */
         XtAddCallback (zone_fsb, XmNokCallback, fsbOkCB,
               (XtPointer) NULL);

         XtAddCallback (zone_fsb, XmNcancelCallback, fsbCancelCB,
            (XtPointer) NULL);

         XtManageChild (zone_fsb);   /* Show the file selection box */
         break;

      case SET_TIMER_BTN:
         settimer();
         break;

      case ID_NOW_BTN:
         /* create thread so clock/flash/beep continue to run */
         ret = pthread_create (&thread, NULL,
                               (void *)cw_send_str, (void *) appRes.cwStr);
         if (ret != 0)
         {
            errorDiag (clock_shell, "popup_cb(): pthread_create failed\n",
                       !CANCEL_BTN);
            fprintf (stderr, "popup_cb(): pthread_create failed\n");
         }
         break;

      case HELP_BTN:
         /* let the cascade callback do it */
         break;

      case QRT:
         cw_exit ();
         exit(0);
         break;

      default:
         errorDiag (clock_shell, "Internal error\n"
                                 "Invalid menu button\n",
                                 !CANCEL_BTN);
         fprintf (stderr, "twclock: Invalid menu button\n");
         break;
   }
}
 

/*
 * callback for About Cascade button
 * switch on the button number
 */
void help_cas_cb(Widget w, XtPointer client_data, XtPointer cbs)
{
   long btn = (long) client_data;

   switch (btn)
   {
      case ABOUT_DIAG:
         popup_about ();
         break;

      case HELP_DIAG:
         popup_help ();
         break;

      default:
         errorDiag (clock_shell, "Internal error\n"
                                 "Invalid button number\n",
                                 !CANCEL_BTN);
         fprintf (stderr, "twclock: Invalid button number");
         break;
   }
}


/*
 * GETTIME FUNCTION
 * A call to this function is hardcoded near the end of widget
 * creation.  At the end of this function, it registers a timeout
 * that forces itself to be called again in one second. And we're off!
 *
 * This function calls localtime(), which bases time on TZ.  The
 * time values are then set into the widgets.  No need to put the
 * same string in the date label 86400 times a day.  So, if the day
 * of the week value has changed, change the label string.  Or, if
 * doit is true, we update the label.  popup_cb() sets doit when
 * the time zone is changed, which forces updating the label.
 */
void  gettime ()
{
   time_t current;
   struct tm *timestruct;
   char  datestr[40];
   static int oldday;
   XmString labelstr;
   unsigned long time_delta = 1000;

   /* register the time out again */
   time_id=XtAppAddTimeOut(app,
      time_delta,
      (XtTimerCallbackProc)gettime,
      (XtPointer)NULL);
   
   putenv (timepath);                   /* put selected time in TZ */
   time (&current);
   timestruct = localtime (&current);   /* get localtime as per TZ */

   XtVaSetValues (hr_scale,  XmNvalue, timestruct->tm_hour, NULL);
   XtVaSetValues (min_scale, XmNvalue, timestruct->tm_min,  NULL);
   XtVaSetValues (sec_scale, XmNvalue, timestruct->tm_sec,  NULL);
   if ((timestruct->tm_mday != oldday) | doit)
   {
      strftime (datestr, sizeof(datestr), "%a %e %b %Y %Z", timestruct);
      labelstr = XmStringCreateLocalized (datestr);

      XtVaSetValues (date_label,
         XmNlabelString, labelstr,
         NULL);

      XmStringFree (labelstr);
      oldday = timestruct->tm_mday;
      doit = 0;
   }
}


/*
 * FSB OK CALLBACK
 * This function gets data from the fsb and builds a string of
 * the form TZ=pathname.  If the pathname ends with a "/",
 * then you didn't select a city in the fsb, so exit and leave
 * TZ alone.  Otherwise, set TZ to the new timezone and set doit.
 * "doit" forces gettime() to use the new TZ and update the date label.
 */
void fsbOkCB (Widget w, XtPointer client_data, XtPointer cbs)
{
   XmFileSelectionBoxCallbackStruct *fsbcbs =
      (XmFileSelectionBoxCallbackStruct *) cbs;

   int  len;
   char *path;

   XmStringGetLtoR (fsbcbs->value, XmFONTLIST_DEFAULT_TAG, &path);

   len = strlen (path);
   if (path[len-1] != '/')       /* If the path ends with /, then do nothing */
   {
      strcpy (timepath, "TZ=");  /* build the new TZ value */
      strcat (timepath, path);   /* and put it in timepath */
      doit = 1;                  /* force date label to be updated next time */
   }
   /* XtUnmanageChild (w); */
   XtDestroyWidget (XtParent (w)); /* Remove the fsb */
   XtFree (path);
}


/*
 * FSB CANCEL CALLBACK
 * Cancel button was pressed, so unmanage the file selection box
 */
void fsbCancelCB (Widget w, XtPointer client_data, XtPointer cbs)
{
   XtDestroyWidget(XtParent (w));
}


/*
 * TBCHANGECB CALLBACK FUNCTION
 * Registers or unregisters the alarm timeout, depending on the state of the
 * toggle button.
 */
void TBchangeCB(Widget w, XtPointer cdata, XtPointer cbs)
{
   XmToggleButtonCallbackStruct *alarmCBS=(XmToggleButtonCallbackStruct *)cbs;

   count = 0;        /* reset flags */
   cwIDdone = False;

   switch(alarmCBS->set)
   {
      case True:
         flashFlag = True;
         /* Save the bg color */
         XtVaGetValues (w,
            XmNbackground, &saveColor,
            NULL);

         /* add a timeout for selected delay */
         alarm_id = XtAppAddTimeOut(app,
            (unsigned long) (((60 * appRes.minutes) + appRes.seconds) * 1000),
            (XtTimerCallbackProc)alarm_timer,
            NULL);

         break;

      case False:
         /* Restore original background */
         XtVaSetValues (w,
            XmNbackground, saveColor,
            NULL);

         /* Remove the alarm time out*/
         XtRemoveTimeOut (alarm_id);

         /* if auto_id != 0, then one exists, so remove it too. */
         if (auto_id != 0)
         {
            XtRemoveTimeOut (auto_id);
         }
         break;

      default:
         errorDiag (clock_shell, "Internal error\n"
                                 "Invalid toggle button state\n",
                                 !CANCEL_BTN);
         fprintf (stderr, "twclock: invalid toggle button state\n");
         break;
  }
} 

/*
 * ALARM TIMER FUNCTION
 * First called when the ID alarm_timer times out.  It will cw id, blink the
 * toggle button and beep as requested in the resource file/setup page.  It
 * then changes the delay to 1 sec and registers another timeout.
 * Unsetting the toggle button unregisters the timeouts.
 */
void alarm_timer (void)
{
   int ret;
   pthread_t thread;
   FILE *con;

   auto_id = 0;  /* so TBchangeCB won't try to unregister it */

   if (appRes.autoReset == True)
   {
      if (count == 0)  /* reset timeout first thing, so no time skew */
      {
         alarm_id = XtAppAddTimeOut(app,
            (unsigned long) (((60 * appRes.minutes) + appRes.seconds) * 1000),
            (XtTimerCallbackProc)alarm_timer,
            (XtPointer)NULL);
      }

      if (count >= appRes.autobeeps)
      {
         /* restore bg color */
         XtVaSetValues (call_toggleB,
            XmNbackground, saveColor,
            NULL);

         /* set up flags and wait for next alarm timeout */
         count = 0;
         cwIDdone = False;
         flashFlag = True;
         return;
      }
      else
      {
         count++;  /* autoReset is True so count. */
      }
   }

   /* If we get here, popup the clock, in case its hidden 
    * Send cw id, flash and blink (if we should) and then
    * set a one second timeout
    */
   XtPopup (clock_shell, XtGrabNone);

   if (appRes.cwID == True && cwIDdone == False)
   {
      /* create thread so clock/flash/beep continue to run */
      ret = pthread_create (&thread, NULL,
                            (void *) cw_send_str, (void *) appRes.cwStr);
      if (ret != 0)
      {
         errorDiag (clock_shell, "popup_cb(): pthread_create failed\n",
                    !CANCEL_BTN);
         fprintf (stderr, "popup_cb(): pthread_create failed\n");
      }
      cwIDdone = True;
   }

   /* should we beep? */
   if (appRes.beep == True)
   {  
      //TJW  implement beep using soundcard
      extern int tonegen (int tone, float time);
      tonegen (700, 250);
   }

   /* should we blink? */
   if (appRes.blink == True && flashFlag == True)
   {
      XtVaSetValues (call_toggleB,
         XmNbackground, appRes.flashColor,
         NULL);
      flashFlag = False;
   }
   else
   {
      XtVaSetValues (call_toggleB,
         XmNbackground, saveColor,
         NULL);
      flashFlag = True;
      
   }
   /* reset auto reset alarm for one second */
   auto_id=XtAppAddTimeOut(app,
      1000L,
      (XtTimerCallbackProc)alarm_timer,
      NULL);   
}


/*
 * SETTIMER FUNCTION
 * Create a dialog box to input new time
 * Add callbacks for min, sec, and close
 */
void settimer (void)
{
   char minutes[10], seconds[10];
   static Widget settimerDiag = NULL;
   static Widget timeTFs[2];
   /* Timer */
   Widget label1;
   Widget frame1, timeForm, min_label, sec_label;
   /* Alarms */
   Widget label2, form2;
   Widget frame2a, form2a, autoResetTB, blinkTB;
   Widget frame2b, form2b, alarmRC, cwidTB, beepTB;
   /* CW ID */
   Widget label3;
   Widget frame3, cwRC, cwSpdScale, cwToneScale;
   Widget sep1, okPB, cancelPB;
   Widget label4, label5;
   XmString xms;

   /* Convert the current minutes and seconds to strings */ 
   sprintf (minutes, "%d", appRes.minutes);
   sprintf (seconds, "%d", appRes.seconds);

   /* Lazy Instantiation */   
   if (settimerDiag != NULL)
   {
#if 0
      XtVaSetValues (timeTFs[0],
         XmNvalue, minutes,
         NULL);
      XtVaSetValues (timeTFs[1],
         XmNvalue, seconds,
         NULL);
#endif
      XtManageChild (settimerDiag);
      return;
   }

   /* Create a message box dialog and set the dialog shell values */   
   settimerDiag = XmCreateFormDialog (clock_shell, "settimerDiag", NULL, 0);

   XtVaSetValues (XtParent(settimerDiag),
      XmNtitle, "TWCLOCK OPTIONS",
   #if (MAKE_ICON == 1) && (HAVE_X11_XPM_H == 1) && (HAVE_LIBXPM == 1)
      XmNiconPixmap, pixmap,
   #endif
      NULL); 

   XtVaSetValues (settimerDiag,          
      XmNdialogStyle, XmDIALOG_MODELESS,
      XmNverticalSpacing, 10,
      XmNhorizontalSpacing, 10,
      XmNnoResize, True,
      NULL);

   xms = XmStringCreateLocalized ("Timer");
   label1 = XtVaCreateManagedWidget ("label1", xmLabelWidgetClass,
      settimerDiag,
      XmNlabelString, xms,
      XmNtopAttachment, XmATTACH_FORM,
      XmNleftAttachment, XmATTACH_FORM,
      XmNbottomAttachment, XmATTACH_NONE,
      XmNrightAttachment, XmATTACH_FORM,
      NULL);
   XmStringFree(xms);

   frame1 = XtVaCreateManagedWidget ("frame1", xmFrameWidgetClass, settimerDiag,
      XmNshadowType, XmSHADOW_OUT,
      XmNshadowThickness, 3,
      XmNtopAttachment, XmATTACH_WIDGET,
      XmNtopWidget, label1,
      XmNleftAttachment, XmATTACH_FORM,
      XmNbottomAttachment, XmATTACH_NONE,
      XmNrightAttachment, XmATTACH_FORM,
      NULL);

   timeForm = XtVaCreateWidget ("timeForm", xmFormWidgetClass, frame1,
      NULL);

   min_label = XtVaCreateManagedWidget ("Minutes", xmLabelWidgetClass,
      timeForm,
      XmNtopAttachment, XmATTACH_FORM,
      XmNleftAttachment, XmATTACH_POSITION,
      XmNleftPosition, 10,
      XmNbottomAttachment, XmATTACH_FORM,
      XmNrightAttachment, XmATTACH_NONE,
      NULL);

   timeTFs[0] = XtVaCreateManagedWidget ("settimerDiag",
      xmTextFieldWidgetClass, timeForm,
      XmNtopAttachment, XmATTACH_FORM,
      XmNleftAttachment, XmATTACH_WIDGET,
      XmNleftWidget, min_label,
      XmNbottomAttachment, XmATTACH_FORM,
      XmNrightAttachment, XmATTACH_POSITION,
      XmNrightPosition, 50,
      XmNvalue, minutes,
      XmNmaxLength, 4,
      XmNcolumns, 4,
      NULL);

   sec_label = XtVaCreateManagedWidget ("Seconds",
      xmLabelWidgetClass, timeForm,
      XmNtopAttachment, XmATTACH_FORM,
      XmNleftAttachment, XmATTACH_POSITION,
      XmNleftPosition, 50,
      XmNbottomAttachment, XmATTACH_FORM,
      XmNrightAttachment, XmATTACH_NONE,
      NULL);
   
   timeTFs[1] = XtVaCreateManagedWidget ("settimerDiag",
      xmTextFieldWidgetClass, timeForm,
      XmNtopAttachment, XmATTACH_FORM,
      XmNleftAttachment, XmATTACH_WIDGET,
      XmNleftWidget, sec_label,
      XmNbottomAttachment, XmATTACH_FORM,
      XmNrightAttachment, XmATTACH_POSITION,
      XmNrightPosition, 90,
      XmNvalue, seconds,
      XmNmaxLength, 4,
      XmNcolumns, 4,
      NULL);

   xms = XmStringCreateLocalized ("Alarms");
   label2 = XtVaCreateManagedWidget ("label2", xmLabelWidgetClass,
      settimerDiag,
      XmNlabelString, xms,
      XmNtopAttachment, XmATTACH_WIDGET,
      XmNtopWidget, timeForm,
      XmNleftAttachment, XmATTACH_FORM,
      XmNbottomAttachment, XmATTACH_NONE,
      XmNrightAttachment, XmATTACH_FORM,
      NULL);
   XmStringFree(xms);

   form2 = XtVaCreateWidget ("form2", xmFormWidgetClass,
      settimerDiag,
      XmNtopAttachment, XmATTACH_WIDGET,
      XmNtopWidget, label2,
      XmNleftAttachment, XmATTACH_FORM,
      XmNbottomAttachment, XmATTACH_NONE,
      XmNrightAttachment, XmATTACH_FORM,
      NULL);

   frame2a = XtVaCreateManagedWidget ("frame2a", xmFrameWidgetClass,
      form2,
      XmNshadowType, XmSHADOW_OUT,
      XmNshadowThickness, 3,
      XmNtopAttachment, XmATTACH_FORM,
      XmNleftAttachment, XmATTACH_FORM,
      XmNbottomAttachment, XmATTACH_NONE,
      XmNrightAttachment, XmATTACH_NONE,
      NULL);

   form2a = XtVaCreateWidget ("form2a", xmFormWidgetClass, frame2a,
      NULL);

   xms = XmStringCreateLocalized ("Auto Reset");
   autoResetTB = XtVaCreateManagedWidget ("autoResetTB",
      xmToggleButtonWidgetClass, form2a,
      XmNlabelString, xms,
      XmNtopAttachment, XmATTACH_FORM,
      XmNleftAttachment, XmATTACH_FORM,
      XmNbottomAttachment, XmATTACH_FORM,
      XmNrightAttachment, XmATTACH_NONE,
      NULL);
   XmStringFree(xms);
   /* sync auoResetTB with autoReset resource */
   XmToggleButtonSetState (autoResetTB, appRes.autoReset, False);

   xms = XmStringCreateLocalized ("Blink");
   blinkTB = XtVaCreateManagedWidget ("blinkTB", xmToggleButtonWidgetClass,
      form2a,
      XmNlabelString, xms,
      XmNtopAttachment, XmATTACH_FORM,
      XmNleftAttachment, XmATTACH_WIDGET,
      XmNleftWidget, autoResetTB,
      XmNbottomAttachment, XmATTACH_FORM,
      XmNrightAttachment, XmATTACH_FORM,
      NULL);
   XmStringFree(xms);
   /* sync blinkTB with blink resource */
   XmToggleButtonSetState (blinkTB, appRes.blink, False);

   frame2b = XtVaCreateManagedWidget ("frame2b", xmFrameWidgetClass,
      form2,
      XmNshadowType, XmSHADOW_OUT,
      XmNshadowThickness, 3,
      XmNtopAttachment, XmATTACH_FORM,
      XmNleftAttachment, XmATTACH_WIDGET,
      XmNleftWidget, frame2a,
      XmNbottomAttachment, XmATTACH_OPPOSITE_WIDGET,
      XmNbottomWidget, frame2a,
      XmNrightAttachment, XmATTACH_FORM,
      NULL);

   form2b = XtVaCreateWidget ("form2b", xmFormWidgetClass, frame2b,
      NULL);

   alarmRC = XtVaCreateManagedWidget ("alarmRC", xmRowColumnWidgetClass,
      form2b,
      XmNradioBehavior, True,
      XmNorientation, XmHORIZONTAL,
      XmNtopAttachment, XmATTACH_FORM,
      XmNleftAttachment, XmATTACH_WIDGET,
      XmNleftWidget, blinkTB,
      XmNbottomAttachment, XmATTACH_FORM,
      XmNrightAttachment, XmATTACH_FORM,
      NULL);

   xms = XmStringCreateLocalized ("CW ID");
   cwidTB = XtVaCreateManagedWidget ("cwidTB", xmToggleButtonWidgetClass,
      alarmRC,
      XmNindicatorType, XmONE_OF_MANY,
      XmNlabelString, xms,
      NULL);
   XmStringFree(xms);
   /* sync cwidTB with cwid resource */
   XmToggleButtonSetState (cwidTB, appRes.cwID, False);

   xms = XmStringCreateLocalized ("Beep");
   beepTB = XtVaCreateManagedWidget ("beepTB", xmToggleButtonWidgetClass,
      alarmRC,
      XmNindicatorType, XmONE_OF_MANY,
      XmNlabelString, xms,
      NULL);
   XmStringFree(xms);
   /* sync beepTB with beep resource in case both are */
   XmToggleButtonSetState (beepTB, appRes.beep, False);


   xms = XmStringCreateLocalized ("CW ID");
   label3 = XtVaCreateManagedWidget ("label3", xmLabelWidgetClass,
      settimerDiag,
      XmNlabelString, xms,
      XmNtopAttachment, XmATTACH_WIDGET,
      XmNtopWidget, frame2a,
      XmNleftAttachment, XmATTACH_FORM,
      XmNbottomAttachment, XmATTACH_NONE,
      XmNrightAttachment, XmATTACH_FORM,
      NULL);
   XmStringFree(xms);

   frame3 = XtVaCreateManagedWidget ("frame3", xmFrameWidgetClass, settimerDiag,
      XmNshadowType, XmSHADOW_OUT,
      XmNshadowThickness, 3,
      XmNmarginWidth, 5,
      XmNmarginHeight, 5,
      XmNtopAttachment, XmATTACH_WIDGET,
      XmNtopWidget, label3,
      XmNleftAttachment, XmATTACH_FORM,
      XmNbottomAttachment, XmATTACH_NONE,
      XmNrightAttachment, XmATTACH_FORM,
      NULL);

   cwRC = XtVaCreateWidget ("cwRC", xmRowColumnWidgetClass, frame3,
      NULL);

   xms = XmStringCreateLocalized ("CW Speed");
    
   /* label for CW speed */
   label4 = XtVaCreateManagedWidget ("label3", xmLabelWidgetClass, cwRC,
      XmNlabelString, xms,
      NULL);

   cwSpdScale = XtVaCreateManagedWidget ("cwSpdScale", xmScaleWidgetClass,
      cwRC,
      XmNscaleMultiple, 5,
      XmNorientation, XmHORIZONTAL,
      XmNshowValue, True,
      XmNminimum, 10,
      XmNmaximum, 30,
      XmNvalue, appRes.cwSpeed,
      XmNshowArrows, XmEACH_SIDE,
      NULL);
   /* free the string and sync cwSpdScale with cwSpeed resource */
   XmStringFree(xms);

   /* label for CW Tone */
   xms = XmStringCreateLocalized ("CW Tone");
   label5 = XtVaCreateManagedWidget ("label5", xmLabelWidgetClass, cwRC,
      XmNlabelString, xms,
      NULL);
   /* free the string */
   XmStringFree(xms);

   cwToneScale = XtVaCreateManagedWidget ("cwToneScale", xmScaleWidgetClass,
      cwRC,
      XmNorientation, XmHORIZONTAL,
      XmNscaleMultiple, 10,
      XmNshowValue, True,
      XmNminimum, 500,
      XmNmaximum, 1300,
      XmNvalue, appRes.cwTone,
      XmNshowArrows, XmEACH_SIDE,
      NULL);

   sep1 = XtVaCreateManagedWidget ("sep1", xmSeparatorWidgetClass, settimerDiag,
      XmNtopAttachment, XmATTACH_WIDGET,
      XmNtopWidget, frame3,
      XmNleftAttachment, XmATTACH_FORM,
      XmNbottomAttachment, XmATTACH_NONE,
      XmNrightAttachment, XmATTACH_FORM,
      NULL);

   okPB = XtVaCreateManagedWidget ("Ok", xmPushButtonWidgetClass, settimerDiag,
      XmNtopAttachment, XmATTACH_WIDGET,
      XmNtopWidget, sep1,
      XmNleftAttachment, XmATTACH_FORM,
      XmNbottomAttachment, XmATTACH_FORM,
      XmNrightAttachment, XmATTACH_POSITION,
      XmNrightPosition, 20,
      NULL);

   cancelPB = XtVaCreateManagedWidget ("Cancel", xmPushButtonWidgetClass,
      settimerDiag,
      XmNtopAttachment, XmATTACH_WIDGET,
      XmNtopWidget, sep1,
      XmNleftAttachment, XmATTACH_POSITION,
      XmNleftPosition, 80,
      XmNbottomAttachment, XmATTACH_FORM,
      XmNrightAttachment, XmATTACH_FORM,
      NULL);

   /* Add the callbacks and 2 modifyVerify */
   XtAddCallback (okPB, XmNactivateCallback, timerOkCB,
               (XtPointer) timeTFs);

   XtAddCallback (timeTFs[0], XmNmodifyVerifyCallback, timerModVerCB,
               (XtPointer) NULL);

   XtAddCallback (timeTFs[1], XmNmodifyVerifyCallback, timerModVerCB,
               (XtPointer) NULL);

   /*
    * TB's all use the same CB function. Client data is a pointer
    * to the resourse that needs tweaking.
    */
   XtAddCallback (autoResetTB, XmNvalueChangedCallback, optionsCB,
               (XtPointer) &appRes.autoReset);

   XtAddCallback (cwidTB, XmNvalueChangedCallback, optionsCB,
               (XtPointer) &appRes.cwID);

   XtAddCallback (beepTB, XmNvalueChangedCallback, optionsCB,
               (XtPointer) &appRes.beep);

   XtAddCallback (blinkTB, XmNvalueChangedCallback, optionsCB,
               (XtPointer) &appRes.blink);

   /*
    * Scales all use the same CB function. Client data is a pointer
    * to the resourse that needs tweaking.
    */
   XtAddCallback (cwSpdScale, XmNdragCallback, scalesCB,
               (XtPointer) 0);

   XtAddCallback (cwSpdScale, XmNvalueChangedCallback, scalesCB,
               (XtPointer) 0);

   XtAddCallback (cwToneScale, XmNdragCallback, scalesCB,
               (XtPointer) 1);

   XtAddCallback (cwToneScale, XmNvalueChangedCallback, scalesCB,
               (XtPointer) 1);

   /* Manage all the dialog kids */
   XtManageChild (timeForm);
   XtManageChild (form2a);
   XtManageChild (alarmRC);
   XtManageChild (form2b);
   XtManageChild (form2);
   XtManageChild (cwRC);
   XtManageChild (settimerDiag);
}


/*
 * Callbacks for setting the alarm_timer
 */


/*
 * OK button for settimer Dialog
 * If the call_toggleB is set, and the time was changed, then
 * calculate and register the new alarm time.
 */
void timerOkCB (Widget w, XtPointer client_data, XtPointer cbs)
{
   Widget *wids = (Widget *) client_data;
   char *minStr, *secStr;
   int min, sec;

   if (newTimeFlag == True)
   {
      minStr = XmTextFieldGetString (wids[0]);
      secStr = XmTextFieldGetString (wids[1]);

      min = atoi(minStr);
      sec = atoi(secStr);

      /* just in case */
      min = min + (sec / 60);
      sec = sec % 60;

      appRes.minutes = min;
      appRes.seconds = sec;
   }

   if (XmToggleButtonGetState (call_toggleB) == True && newTimeFlag == True)
   {
      XtRemoveTimeOut (alarm_id);
      alarm_id = XtAppAddTimeOut(app,
         (unsigned long) (((60 * appRes.minutes) + appRes.seconds) * 1000),
         (XtTimerCallbackProc)alarm_timer, (XtPointer)NULL);
   }
   /* finally, clear the flag */
   newTimeFlag = False;
}


/*
 * modify/verify callback for time textfields
 * only allow numbers in the min and sec fields
 * if time is change is valid, set the the newTimeFlag
 */
void timerModVerCB (Widget w, XtPointer client_data, XtPointer cbs)
{
   int i;
   XmTextVerifyCallbackStruct *pt = (XmTextVerifyCallbackStruct *) cbs;

   for (i=0; i < pt->text->length; i++)
   {
      if (isdigit (pt->text->ptr[i]))  /* allow only digits */
      {
         newTimeFlag = True;
         return;
      }
      else
      {
         pt->doit = False;
         newTimeFlag = False;
         return;
      }
   }
}


/*
 * optionsCB value changed callback for option toggle buttons
 * maintains the values of alarm option resources
 * client_data is a pointer to appRes where state is stored
 */
void optionsCB (Widget w, XtPointer client_data, XtPointer cbs)
{
   int *ptr = (int *)client_data;
   XmToggleButtonCallbackStruct *cbsptr = (XmToggleButtonCallbackStruct *) cbs;

   *ptr = cbsptr->set;
}


/*
 * scalesCB drag/valueChanged callbacks for CW ID scales
 * client_data is 0 for speed scale and 1 for tone
 */
void scalesCB (Widget w, XtPointer client_data, XtPointer cbs)
{
   int val;
   long k = (long)client_data;
   XmScaleCallbackStruct *cbsptr = (XmScaleCallbackStruct *) cbs;

   val = cbsptr->value;

   switch (k)
   {
      case 0:
         appRes.cwSpeed = val;
         cw_set_wpm (appRes.cwSpeed);
         break;

      case 1:
         appRes.cwTone = val;
         cw_set_tone (appRes.cwTone);  
         break;

      default:
         errorDiag (clock_shell, "Internal error\n"
                                 "Invalid scalesCB argument\n",
                                 !CANCEL_BTN);
         fprintf (stderr, "twclock: invalid argument\n");
   }
}
