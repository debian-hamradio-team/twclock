/*
 * twclock:  A world clock implemented with openMotif widgets
 * Copyright (C) 1997-2020 Ted Williams - WA0EIR
 *
 * This file is part of twclock
 *
 * twclock is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have recieved a copy of the GNU General Public License
 * along with this program. If not, see <http://gnu.org/licences/>.
 *
 * Versions: 3.5 -  Jan 2020
 */

/*
 * twclockDiags.c
 */
#include "twclock.h"
#include "twclockHelp.h"

#if (MAKE_ICON == 1) && (HAVE_X11_XPM_H == 1) && (HAVE_LIBXPM == 1)
extern Pixmap pixmap;
#endif

/*
 * aboutDiag Function
 * Just an about dialog box
 */
void popup_about()
{
   static Widget diag = NULL;
   Widget pb;
   XmString msg_xs;
   char msg_str[] =
      PACKAGE_STRING
      " \251 1997-2020 was written by\nTed Williams - WA\330EIR\n\n"
      "Please send bug reports to\n" PACKAGE_BUGREPORT;

   if (diag == NULL)
   {
      msg_xs = XmStringCreateLtoR (msg_str, XmSTRING_DEFAULT_CHARSET);

      diag = XmCreateInformationDialog (clock_shell, "aboutDiag",
        (ArgList) NULL, 0);

      XtVaSetValues (XtParent (diag),
         XmNtitle, "ABOUT TWCLOCK",
      #if (MAKE_ICON == 1) && (HAVE_X11_XPM_H == 1) && (HAVE_LIBXPM == 1)
         XmNiconPixmap, pixmap,
      #endif
         NULL);

      XtVaSetValues (diag,
         XmNmessageAlignment, XmALIGNMENT_CENTER,
         XmNmessageString, msg_xs,
         NULL);
      XmStringFree (msg_xs);

      XtUnmanageChild (XmMessageBoxGetChild (diag, XmDIALOG_CANCEL_BUTTON));
      XtUnmanageChild (XmMessageBoxGetChild (diag, XmDIALOG_HELP_BUTTON));
      XtUnmanageChild (XmMessageBoxGetChild (diag, XmDIALOG_OK_BUTTON));

      pb = XtVaCreateManagedWidget ("OK", xmPushButtonWidgetClass, diag,
         NULL);
   }
   XtManageChild (diag);
}


/*
 * Help Dialog
 */
void popup_help (void)
{
   int i;
   static Widget helpDiag = 0;
   static Widget helpLabel, closePB, sep, helpTextSW;
   Arg args[20];

   if (helpDiag == NULL)
   {
      helpDiag = XmCreateFormDialog (clock_shell, "helpDiag", NULL, 0);

      XtVaSetValues (helpDiag,
        XmNdialogStyle, XmDIALOG_MODELESS,
        XmNverticalSpacing, 10,
        XmNhorizontalSpacing, 10,
        NULL);

      XtVaSetValues (XtParent (helpDiag),       /* set title bar */
         XmNtitle, "TWCLOCK HELP",
      #if (MAKE_ICON == 1) && (HAVE_X11_XPM_H == 1) && (HAVE_LIBXPM == 1)
         XmNiconPixmap, pixmap,
      #endif
         NULL);

      helpLabel = XtVaCreateManagedWidget ("TWCLOCK HELP", xmLabelWidgetClass,
         helpDiag,
         XmNtopAttachment, XmATTACH_FORM,
         XmNleftAttachment, XmATTACH_FORM,
         XmNbottomAttachment, XmATTACH_NONE,
         XmNrightAttachment, XmATTACH_FORM,
         NULL);

      closePB = XtVaCreateManagedWidget ("Close", xmPushButtonWidgetClass,
         helpDiag,
         XmNtopAttachment, XmATTACH_NONE,
         XmNleftAttachment, XmATTACH_POSITION,
         XmNleftPosition, 40,
         XmNrightAttachment, XmATTACH_POSITION,
         XmNrightPosition, 60,
         XmNbottomAttachment, XmATTACH_FORM,
         NULL);

      sep = XtVaCreateManagedWidget ("sep", xmSeparatorWidgetClass, helpDiag,
         XmNtopAttachment, XmATTACH_NONE,
         XmNleftAttachment, XmATTACH_FORM,
         XmNbottomAttachment, XmATTACH_WIDGET,
         XmNbottomWidget, closePB,
         XmNrightAttachment, XmATTACH_FORM,
         NULL);

      i = 0;
      XtSetArg (args[i], XmNrows, 12);  i++;
      XtSetArg (args[i], XmNcolumns, 80);  i++;
      XtSetArg (args[i], XmNeditMode, XmMULTI_LINE_EDIT);  i++;
      XtSetArg (args[i], XmNeditable, False);  i++;
      XtSetArg (args[i], XmNscrollHorizontal, False);  i++;
      XtSetArg (args[i], XmNwordWrap, True);  i++;
      XtSetArg (args[i], XmNtopAttachment, XmATTACH_WIDGET);  i++;
      XtSetArg (args[i], XmNtopWidget, helpLabel);  i++;
      XtSetArg (args[i], XmNleftAttachment, XmATTACH_FORM);  i++;
      XtSetArg (args[i], XmNbottomAttachment, XmATTACH_WIDGET);  i++;
      XtSetArg (args[i], XmNbottomWidget, sep);  i++;
      XtSetArg (args[i], XmNrightAttachment, XmATTACH_FORM);  i++;
      XtSetArg (args[i], XmNvalue, helpText); i++;

      helpTextSW = XmCreateScrolledText (helpDiag, "helpTextSW", args, i);

      XtManageChild (helpTextSW);

   }
   XtManageChild (helpDiag);    /* pop it up */
}


/*
 * Error Dialog - creates an error dialog with either an OK or Exit button.
 * Called from many places and passed the parent widget, the error message
 * to display and a flag that indicates if there should be an OK button
 * or a Cancel/Exit button.
 */
void errorDiag (Widget w, char *emess, int flag)
{
   Widget errDiag;
   XmString xs_emess, xs_cancel;

   errDiag = XmCreateErrorDialog (w, "errDiag", NULL, 0);
   xs_emess = XmStringCreateLocalized(emess);
   xs_cancel = XmStringCreateLocalized ("Exit");

   XtVaSetValues (XtParent(errDiag),
      XmNtitle, "TWCLOCK ERROR",
   #if (MAKE_ICON == 1) && (HAVE_X11_XPM_H == 1) && (HAVE_LIBXPM == 1)
      XmNiconPixmap, pixmap,
   #endif
      XmNmwmFunctions, MWM_FUNC_ALL | MWM_FUNC_CLOSE,
      NULL);

   XtVaSetValues (errDiag,
      XmNdialogStyle, XmDIALOG_PRIMARY_APPLICATION_MODAL,
      XmNcancelLabelString, xs_cancel,
      XmNmessageString, xs_emess,
      NULL);
   XmStringFree (xs_emess);

   XtUnmanageChild (XmMessageBoxGetChild(errDiag, XmDIALOG_HELP_BUTTON));

   XtAddCallback (errDiag, XmNokCallback, errOkCB, w);

   if (flag == CANCEL_BTN)
   {
      XtUnmanageChild (XmMessageBoxGetChild(errDiag, XmDIALOG_OK_BUTTON));
      XtAddCallback (errDiag, XmNcancelCallback, errCancelCB, w);
   }
   else
   {
      XtUnmanageChild (XmMessageBoxGetChild(errDiag, XmDIALOG_CANCEL_BUTTON));
      XtAddCallback (errDiag, XmNcancelCallback, errOkCB, w);
   }

   XtManageChild (errDiag);
}


/*
 * errOkCB - cdata is wid of convert diag
 * which needs to be unmanaged now.
 */
void errOkCB (Widget w, XtPointer cdata, XtPointer cbs)
{
   /* nothing to do */
} 


/*
 * errCancelCB - unmanage the shell to exit
 */
void errCancelCB (Widget w, XtPointer cdata, XtPointer cbs)
{
   XtUnmanageChild (cdata);
} 

