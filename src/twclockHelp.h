/*
 * twclock:  A world clock implemented with openMotif widgets
 * Copyright (C) 1997-2020 Ted Williams - WA0EIR
 *
 * This file is part of twclock
 *
 * twclock is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have recieved a copy of the GNU General Public License
 * along with this program. If not, see <http://gnu.org/licences/>.
 *
 * Versions: 3.5 -  Jan 2020
 */

/*
 * twclockHelp.h
 */
char helpText[] = " \n\
                CONTENTS\n\n\
I     ADDITIONAL INSTALLATION NOTES\n\
II    DESCRIPTION \n\
III   USER INTERFACE \n\
\n\n\
I    ADDITIONAL INSTALLATION NOTES\n\
\n\
   See the INSTALL file for basic install instructions. It is the standard \
\"./configure\", \"make\", and \"make install\" commands. Once that \
is done, you will want to follow the process below.\n\
\n\
   The \"make install\" command put a copy of the resource file (Twclock) \
in one of many places where X looks for resource files on your system.  \
To find where Twclock was installed, you can run \"make install\" again and \
look near the bottom of the output.  There will be a line like: \n\
\n\
   Installing Twclock in /etc/X11/app-defaults\n\
\n\
In this case, it was installed in the /etc/X11/app-default directory.\n\
\n\
You will want to make changes to Twclock to tailor it to your needs.  \
The easiest way is to edit the copy of Twclock just installed \
by \"make install\".  But, you must be root!\n\
\n\
You can also: \
\n\
   copy Twclock to your home directory.\n\n\
   -OR-\n\n\
   cat it to the end of your .Xdefaults file.\n\n\
  -OR-\n\n\
   set the XUSERFILESEARCHPATH environmental variable and\n\
   copy Twclock to your own resources directory.\n\n\
   To use XUSERFILESEARCHPATH, follow these steps:\n\
   1) Edit the .profile file in your home directory and add\n\
      the line below:\n\
         export XUSERFILESEARCHPATH=$HOME/app-defaults/%N \n\
   2) Create an app-defaults directory in your home directory.\n\
   3) Copy resources files to $HOME/app-defaults directory.\n\
\n\
   Now you can edit your copy of Twclock and you don't need to be root. \
There are comments (lines beginning with an !) in Twclock explaining the \
values.  After you change the values, restart the program to have them \
take effect.\n\
\n\
   Here are some of the resources you will want to modify. \n\
\n\
   Twclock.form.call_toggleB.labelString:   your call\n\
   Twclock.cwStr:   de your call\n\
\n\
See http://www.faqs.org/faqs/x-faq/part2/section-22.html or \n\
the X manual pages for more information.\n\
\n\
\n\
II   DESCRIPTION\n\
\n\
  This is a clock program which will prove to be very helpful for \
ham operators and will displays the current time in major cities \
around the world. It also has an ID timer and alarm. The alarm \
can be set to beep, flash or send your call in CW via the sound card. \
The audio is generated using the pulseaudio system and its level \
can be set using the pulseaudio volume controle.\n\
\n\
An alarm will notify you when it is time for a station ID. The \
delay before an ID is set in the resource file (Twclock). It can \
also be set at runtime via a popup menu. The popup menu also lets \
you pick alarm options at runtime. You have a choice of CW ID, blink, \
and beep. There is also a Auto Reset option. This option automatically \
starts another time out without any user action.\n\
\n\
  Local or GMT time can be displayed. In addition, the current \
time at some point on the globe can be displayed by using the time \
zone information contained in the files located under the directory \
/usr/lib/zoneinfo or /usr/share/zoneinfo. A file selection box \
allows you to pick the region and city of interest, and the environment \
variable TZ is set to the pathname for that time zone file. The clock \
will then display the current time for the selected location. This \
only changes TZ for the clock process - other processes are unaffected\n\
\n\
\n\
III   USER INTERFACE\n\
\n\
  The interface has two functions, a Clock and an ID timer.\n\
\n\
     Clock Operation\n\
\n\
  The GUI uses scale widgets to display hours, minutes, and seconds. \
The button at the top is used to start and stop the alarm timer. \
When the time expires, the selected alarm options are performed. \
You can choose to have the button flash, the PC speaker beep and a \
CW ID can be sent via the sound card. See the notes in the resource \
file (Twclock) and the ID Timer Operation section below for more \
information.\n\
\n\
  There is only this one undocumented popup menu (via the right \
mouse button). Two of the menu buttons, Local and GMT set the \
clock to that time. Ctrl-L and Ctrl-G do the same thing as pushing \
the Local and GMT menu buttons.\n\
\n\
  The third menu button, Others: (or Control-O), pops up a file \
selection box dialog that allows you to select an area and city. Then, \
the OK button will set the clock to the selected zone. The Cancel \
button can be pressed at any time to popdown the dialog, leaving the \
time unchanged.\n\
\n\
  The fourth button is the Set Timer button. This button (or Ctrl-S) \
pops up a window where you can set the time for the alarm and alarm \
options. See the ID Timer Operation section below for a complete \
description.\n\
\n\
  The fifth button is ID NOW.  This button will cause a CW id to be \
sent when pushed.\n\
\n\
  Next is the About button. It give you two choices - About and Help. \
About is just an About Dialog box. Help generates the window you're \
reading right now!\n\
\n\
  The last button is labeled QRT. Nuff said about that.\n\
\n\
     ID Timer Operation\n\
\n\
  The ID timer is started with the button at the top of the main window. \
Pushing it in starts the timer out. When the time expires, the alarm goes off. \
There are three alarms - blink the button, beep the PC speaker and send a \
CW ID via the sound card. Press the button again to turn the alarm off.\
Push the button again to start another time out.\n\
\n\
The popup menu (right mouse click) has a Set Timer button which opens a \
Options dialog where you can set timer options. This window can also \
be opened with Control-S. There are three sections on this window and \
it will initially display the default options you defined in the resource \
file (Twclock).\n\
\n\
The Timer section allows you to set a time out interval. You can set the \
delay from 0 (?) to 9999 minutes and 9999 seconds. Push the OK button to \
apply the new value, or push Cancel to leave it unchanged.\n\
\n\
The Alarm section has four option buttons.  Three of the buttons (CW ID, \
Beep, and Blink) allow you to select which type of alarm you want.  The \
fourth button is the Auto Reset option for the timer. When it is enabled, \
a new time out is automatically restarted after each time out. There is no \
need for user action. For example, with Auto Reset on and the timer set for \
10 minutes, pushing the button on the main window will generate an alarm \
every 10 minutes until the main window button is pushed again.\n\
\n\
The CW ID section of the Options window allows you to set the speed and \
tone of the CW ID.  Use the pulseaudio Volume Control application to set \
the cw level. \n\
\n\
\n\
Got any problems, comments, suggestions, etc?\n\
\n\
My email is: wa0eir@wa0eir.bcts.info\n\
\n\
73,\n\
Ted - wa0eir ";


