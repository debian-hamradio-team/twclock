/*
 * twclock:  A world clock implemented with openMotif widgets
 * Copyright (C) 1997-2020 Ted Williams - WA0EIR
 *
 * This file is part of twclock
 *
 * twclock is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have recieved a copy of the GNU General Public License
 * along with this program. If not, see <http://gnu.org/licences/>.
 *
 * Versions: 3.5 -  Jan 2020
 */

/*
 * twclockWids.c
 */
#include "twclock.h"

void  build_widgets ()
{
   Widget form, separator, popup_menu, help_cas_menu;

   XmString xms[20];
   int i,j;

   /*
    * This section creates all of the widgets for the main
    * interface.  Nothing very interesting here - mostly just
    * creates and setting attachment resources.
    */

   /*
    * Create form
    */
   form = XtVaCreateWidget ("form", xmFormWidgetClass, clock_shell,
      NULL);

   /*
    * Create date_label
    */
   date_label = XtVaCreateManagedWidget ("date_label",
      xmLabelWidgetClass, form,
      XmNbottomAttachment, XmATTACH_FORM,
      XmNrightAttachment, XmATTACH_FORM,
      XmNleftAttachment, XmATTACH_FORM,
      XmNtopAttachment, XmATTACH_NONE,
      NULL);

   /*
    * Create separator
    */
   separator = XtVaCreateManagedWidget ("separator",
      xmSeparatorWidgetClass, form,
      XmNbottomAttachment, XmATTACH_WIDGET,
      XmNbottomWidget, date_label,
      XmNrightAttachment, XmATTACH_FORM,
      XmNtopAttachment, XmATTACH_NONE,
      XmNleftAttachment, XmATTACH_FORM,
      XmNshadowThickness, 4,
      XmNseparatorType, XmSHADOW_ETCHED_OUT,
      NULL);

   /*
    * Create call_toggleB
    */
   call_toggleB = XtVaCreateManagedWidget ("call_toggleB",
      xmToggleButtonWidgetClass, form,
      XmNbottomAttachment, XmATTACH_NONE,
      XmNrightAttachment, XmATTACH_FORM,
      XmNleftAttachment, XmATTACH_FORM,
      XmNtopAttachment, XmATTACH_FORM,
      XmNhighlightThickness, 0,   /* these make it look like  */
      XmNshadowThickness, 3,      /* a push on/push off button */
      XmNindicatorOn, XmINDICATOR_NONE,
      XmNfillOnSelect, False,
      NULL);

   /*
    * Create hr_scale
    */
   hr_scale = XtVaCreateManagedWidget ("hr_scale",
      xmScaleWidgetClass, form,
      XmNnavigationType, XmNONE,
      XmNbottomAttachment, XmATTACH_WIDGET,
      XmNbottomWidget, separator,
      XmNrightAttachment, XmATTACH_POSITION,
      XmNrightPosition, 30,
      XmNleftAttachment, XmATTACH_FORM,
      XmNtopAttachment, XmATTACH_WIDGET,
      XmNtopWidget, call_toggleB,
      XmNmaximum, 23,
      XmNeditable, False,
      XmNshowValue, True,
      NULL);

   /*
    * Create min_scale
    */
   min_scale = XtVaCreateManagedWidget ("min_scale",
      xmScaleWidgetClass, form,
      XmNnavigationType, XmNONE,
      XmNrightOffset, 5,
      XmNleftOffset, 5,
      XmNbottomAttachment, XmATTACH_WIDGET,
      XmNbottomWidget, separator,
      XmNleftAttachment, XmATTACH_POSITION,
      XmNleftPosition, 35,
      XmNrightAttachment, XmATTACH_POSITION,
      XmNrightPosition, 65,
      XmNtopAttachment, XmATTACH_WIDGET,
      XmNtopWidget, call_toggleB,
      XmNmaximum, 59,
      XmNeditable, False,
      XmNshowValue, True,
      NULL);

   /*
    * Create sec_scale
    */
   sec_scale = XtVaCreateManagedWidget ("sec_scale",
      xmScaleWidgetClass, form,
      XmNnavigationType, XmNONE,
      XmNbottomAttachment, XmATTACH_WIDGET,
      XmNbottomWidget, separator,
      XmNleftAttachment, XmATTACH_POSITION,
      XmNleftPosition, 70,
      XmNrightAttachment, XmATTACH_FORM,
      XmNtopAttachment, XmATTACH_WIDGET,
      XmNtopWidget, call_toggleB,
      XmNmaximum, 59,
      XmNeditable, False,
      XmNshowValue, True,
      NULL);

   /*
    * Next, we create the popup menu.  All buttons call the
    * function popup_cb.
    *
    * Create the XmStrings for the push buttons
    */

   i = 0;
   xms[i] = XmStringCreateLocalized ("Time Zone"); i++;
   xms[i] = XmStringCreateLocalized ("Local"); i++;
   xms[i] = XmStringCreateLocalized ("Ctrl-L"); i++;
   xms[i] = XmStringCreateLocalized ("GMT"); i++;
   xms[i] = XmStringCreateLocalized ("Ctrl-G"); i++;
   xms[i] = XmStringCreateLocalized ("Others"); i++;
   xms[i] = XmStringCreateLocalized ("Ctrl-O"); i++;
   xms[i] = XmStringCreateLocalized ("Set Timer"); i++;
   xms[i] = XmStringCreateLocalized ("Ctrl-S"); i++;
   xms[i] = XmStringCreateLocalized ("ID Now"); i++;
   xms[i] = XmStringCreateLocalized ("Ctrl-I"); i++;
   xms[i] = XmStringCreateLocalized ("Help"); i++;
   xms[i] = XmStringCreateLocalized ("QRT"); i++;
   xms[i] = XmStringCreateLocalized ("Ctrl-Q"); i++;

   /*
    * Create popup_menu
    */
   //i = 0;
   popup_menu = XmVaCreateSimplePopupMenu (form, "popup_menu", popup_cb,
      XmVaTITLE, xms[0],
      XmVaSEPARATOR,
      XmVaPUSHBUTTON, xms[1], 'L', "Ctrl<Key>L", xms[2],
      XmVaPUSHBUTTON, xms[3], 'G', "Ctrl<Key>G", xms[4],
      XmVaPUSHBUTTON, xms[5], 'O', "Ctrl<Key>O", xms[6],
      XmVaSEPARATOR,
      XmVaPUSHBUTTON, xms[7], 'S', "Ctrl<Key>S", xms[8],
      XmVaSEPARATOR,
      XmVaPUSHBUTTON, xms[9], 'I', "Ctrl<Key>I", xms[10],
      XmVaSEPARATOR,
      XmVaCASCADEBUTTON, xms[11], NULL,
      XmVaSEPARATOR,
      XmVaPUSHBUTTON, xms[12], 'Q', "Ctrl<Key>Q", xms[13],
      NULL);

   /*
    * Prevent memory leaks 
    */
   for (j=0; j<i; j++)
   {
      XmStringFree(xms[j]);
   }

   /*
    * Create cascade menu for the Help cascade button 
    */
   i = 0;
   xms[i] = XmStringCreateLocalized ("About"); i++;
   xms[i] = XmStringCreateLocalized ("Help"); i++;

   help_cas_menu = XmVaCreateSimplePulldownMenu (popup_menu,"help_cas_menu",
      post_from, help_cas_cb,
      XmVaPUSHBUTTON, xms[0], NULL, "", NULL,
      XmVaPUSHBUTTON, xms[1], NULL, "", NULL,
      NULL);

   /*
    * Prevent memory leaks 
    */
   for (j=0; j<i; j++)
   {
      XmStringFree(xms[j]);
   }


/*
 * Here the event handler is registered. The default timezone value is
 * grabbed and a call to popup_cb() is made.  This gets the path variable
 * to the start up time zone.  Then gettime() sets TZ and initializes
 * the scales.
 */
   XtAddEventHandler (form ,ButtonPressMask, False, input_event, popup_menu);

/*
* This is a hack to make the right mouse event be recognized on all the
* children, but the slider in the scale didn't work.  Now they work too
* with the addition of the last three XtAddEventHandler
*/
#ifdef LESSTIF_VERSION 
/* mouse grab bug - force lesstif fix for xorg bug */
/* control keys still won't work cuz no menu bar */
   fprintf (stderr, "doing hack for lesstif\n");
   XtAddEventHandler (date_label,ButtonPressMask,False,input_event,popup_menu);
   XtAddEventHandler (separator,ButtonPressMask,False,input_event,popup_menu);
   XtAddEventHandler (call_toggleB,ButtonPressMask,False,input_event,
	popup_menu);
   XtAddEventHandler (hr_scale,ButtonPressMask,False,input_event,popup_menu);
   XtAddEventHandler (min_scale,ButtonPressMask,False,input_event,popup_menu);
   XtAddEventHandler (sec_scale,ButtonPressMask,False,input_event,popup_menu);
   XtAddEventHandler (XtNameToWidget(hr_scale,"Scrollbar"),ButtonPressMask,
                      False,input_event,popup_menu);
   XtAddEventHandler (XtNameToWidget(min_scale,"Scrollbar"),ButtonPressMask,
                      False,input_event,popup_menu);
   XtAddEventHandler (XtNameToWidget(sec_scale,"Scrollbar"),ButtonPressMask,
                      False,input_event,popup_menu);
#endif

   /* call the popup_cb to set the time to start up xdb value. */
   popup_cb (form, (XtPointer)(appRes.tzone), (XtPointer)NULL);
   gettime ();

   XtManageChild(form);

   /*add callbacks */
   XtAddCallback(call_toggleB, XmNvalueChangedCallback,
                 TBchangeCB, (XtPointer)NULL);

   return;
}
